ConvertGpDataFormat is an extensible utility for converting file data.

Inbuilt conversions include (all customisable):

* Arrange files by class
    * Moves files to folders based on a value (such as class) within the files
* Column statistics
* Combine data sets
* Concatenate data
* Concatenate lines
* Convert data between GP formats
    * Converts data formats between my GP and Davis's GP
* Convert dates to integers
* Count variables in best programs
    * Use with my GP
* Create GP data file
    * Use with my GP
* Create GP script file
    * Use with my GP
* Determine classes
    * Extracts a single column from an input file
* Drop columns
* Extract fields
    * Extracts specific data from a set of files
* Find and replace text
* Find original indices
    * Finds the indices (line number) of data from one file in another
* Fitness after n generations
    * Creates data for a plot of fitness vs. generation, Use with my GP program
* Get CSV data dimensions
* Hash files
* Make bigger bins
    * Use with NMR data binned using Davis's Adaptive Binning program
* NMR bin identification
    * Use with NMR data binned using Davis's Adaptive Binning program
* PPM identification
    * Use with NMR data binned using Davis's Adaptive Binning program
* Progenesis format to MetaboliteLevels format
* Remove data headers
* Rename files
* Single value statistics I
    * Calculates min, max, avg, etc. from data file
* Single value statistics II
    * Finds specific data from a set of files and puts into one file
* Split columns into files
* Split into training and test data
    * Optionally preserves class ratio
* Transpose data set
* Convert between CSV formats (e.g. CSV to TSV)
