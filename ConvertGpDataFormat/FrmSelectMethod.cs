﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertGpDataFormat
{
    public partial class FrmSelectMethod : Form
    {
        private MethodListItem SelectedMethod;

        internal static MethodListItem Show(IWin32Window owner, IEnumerable<MethodListItem> list)
        {
            using (FrmSelectMethod frm = new FrmSelectMethod(list))
            {
                if (frm.ShowDialog(owner) == DialogResult.OK)
                {
                    return frm.SelectedMethod;
                }

                return null;
            }
        }

        internal FrmSelectMethod(IEnumerable<MethodListItem> list)
            : this()
        {
            foreach (var i in list)
            {
                ListViewItem lvi = new ListViewItem(i.GetDisplayName());
                lvi.Tag = i;
                lvi.ImageIndex = 0;
                listView1.Items.Add(lvi);
            }
        }

        public FrmSelectMethod()
        {
            InitializeComponent();
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;

            if (lv.SelectedItems.Count == 1)
            {
                SelectedMethod = (MethodListItem)lv.SelectedItems[0].Tag;
                DialogResult = DialogResult.OK;
            }
        }
    }
}
