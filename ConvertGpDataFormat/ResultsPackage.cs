﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat
{
    public class ResultsPackage
    {
        public class Result
        {
            public string name;
            public string data;
            public ResultsType type;
            
            public Result(string name, string data, ResultsType type)
            {
                this.name = name;
                this.data = data;
                this.type = type;
            }
        }

        List<Result> _results = new List<Result>();
        Action<int, int> progress;

        public ResultsPackage(Action<int, int> progress)
        {
            this.progress = progress;
        }

        public void ReportProgress(int a, int outOf)
        {
            progress(a, outOf);
        }

        public void ReportProgress(StreamReader sr)
        {
            progress((int)sr.BaseStream.Position, (int)sr.BaseStream.Length);
        }

        public void ReportProgress(StreamReader sr, int index, int numFiles)
        {
            int p = (int)sr.BaseStream.Position;
            int l = (int)sr.BaseStream.Length;
            progress(p + (l * index), l * numFiles);
        }
        
        public void Add(string name, string data, ResultsType type)
        {
            _results.Add(new Result(name, data, type));
        }

        public void AddInfo(string title, object value)
        {
            _results.Add(new Result(title, (value ?? "(null)").ToString(), ResultsType.Information));
        }

        public void AddOutput(string fileName)
        {
            _results.Add(new Result("Output", fileName, ResultsType.FileName));
        }

        public void AddOutput(string title, string fileName)
        {
            Add(title, fileName, ResultsType.FileName);
        }

        public IEnumerable<Result> Get()
        {
            return _results;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var r in _results)
            {
                sb.AppendLine(r.name + ": " + r.data);
            }

            return sb.ToString();
        }
    }

    public enum ResultsType
    {
        Information,
        FileName,
        Report
    }
}
