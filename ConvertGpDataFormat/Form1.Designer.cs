﻿namespace ConvertGpDataFormat
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._propertyGrid = new System.Windows.Forms.PropertyGrid();
            this._lstTransformations = new System.Windows.Forms.ComboBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this._btnMenu = new System.Windows.Forms.Button();
            this._btnProcess = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.loadSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lastSavedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usingFileNameMacrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this._propertyGrid, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._lstTransformations, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.progressBar1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(811, 478);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // _propertyGrid
            // 
            this._propertyGrid.BackColor = System.Drawing.Color.Gainsboro;
            this._propertyGrid.CategoryForeColor = System.Drawing.Color.Black;
            this._propertyGrid.CategorySplitterColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.SetColumnSpan(this._propertyGrid, 2);
            this._propertyGrid.CommandsBackColor = System.Drawing.Color.White;
            this._propertyGrid.CommandsBorderColor = System.Drawing.Color.White;
            this._propertyGrid.CommandsDisabledLinkColor = System.Drawing.Color.Gray;
            this._propertyGrid.CommandsForeColor = System.Drawing.Color.Black;
            this._propertyGrid.CommandsLinkColor = System.Drawing.Color.Blue;
            this._propertyGrid.DisabledItemForeColor = System.Drawing.Color.Gray;
            this._propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this._propertyGrid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._propertyGrid.HelpBackColor = System.Drawing.Color.Gainsboro;
            this._propertyGrid.HelpBorderColor = System.Drawing.Color.Gainsboro;
            this._propertyGrid.HelpForeColor = System.Drawing.Color.Black;
            this._propertyGrid.LineColor = System.Drawing.Color.Gainsboro;
            this._propertyGrid.Location = new System.Drawing.Point(14, 74);
            this._propertyGrid.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this._propertyGrid.Name = "_propertyGrid";
            this._propertyGrid.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this._propertyGrid.SelectedItemWithFocusBackColor = System.Drawing.Color.Blue;
            this._propertyGrid.SelectedItemWithFocusForeColor = System.Drawing.Color.White;
            this._propertyGrid.Size = new System.Drawing.Size(783, 351);
            this._propertyGrid.TabIndex = 1;
            this._propertyGrid.ToolbarVisible = false;
            this._propertyGrid.ViewBackColor = System.Drawing.Color.White;
            this._propertyGrid.ViewBorderColor = System.Drawing.Color.Gainsboro;
            this._propertyGrid.ViewForeColor = System.Drawing.Color.Black;
            // 
            // _lstTransformations
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._lstTransformations, 2);
            this._lstTransformations.Dock = System.Windows.Forms.DockStyle.Top;
            this._lstTransformations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._lstTransformations.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._lstTransformations.FormattingEnabled = true;
            this._lstTransformations.Location = new System.Drawing.Point(14, 11);
            this._lstTransformations.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this._lstTransformations.Name = "_lstTransformations";
            this._lstTransformations.Size = new System.Drawing.Size(783, 25);
            this._lstTransformations.TabIndex = 3;
            this._lstTransformations.SelectedIndexChanged += new System.EventHandler(this._lstTransformations_SelectedIndexChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel1.Controls.Add(this._btnMenu);
            this.flowLayoutPanel1.Controls.Add(this._btnProcess);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(448, 432);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(348, 32);
            this.flowLayoutPanel1.TabIndex = 4;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // _btnMenu
            // 
            this._btnMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnMenu.BackColor = System.Drawing.Color.White;
            this._btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._btnMenu.ForeColor = System.Drawing.Color.Black;
            this._btnMenu.Image = ((System.Drawing.Image)(resources.GetObject("_btnMenu.Image")));
            this._btnMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnMenu.Location = new System.Drawing.Point(4, 2);
            this._btnMenu.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this._btnMenu.Name = "_btnMenu";
            this._btnMenu.Size = new System.Drawing.Size(166, 28);
            this._btnMenu.TabIndex = 3;
            this._btnMenu.Text = "Menu";
            this._btnMenu.UseVisualStyleBackColor = false;
            this._btnMenu.Click += new System.EventHandler(this._btnMenu_Click);
            // 
            // _btnProcess
            // 
            this._btnProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._btnProcess.BackColor = System.Drawing.Color.White;
            this._btnProcess.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._btnProcess.ForeColor = System.Drawing.Color.Black;
            this._btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("_btnProcess.Image")));
            this._btnProcess.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._btnProcess.Location = new System.Drawing.Point(178, 2);
            this._btnProcess.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this._btnProcess.Name = "_btnProcess";
            this._btnProcess.Size = new System.Drawing.Size(166, 28);
            this._btnProcess.TabIndex = 2;
            this._btnProcess.Text = "Process";
            this._btnProcess.UseVisualStyleBackColor = false;
            this._btnProcess.Click += new System.EventHandler(this._btnProcess_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Cornsilk;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(14, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(783, 34);
            this.label1.TabIndex = 5;
            this.label1.Text = "\r\n ";
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(13, 430);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(427, 36);
            this.progressBar1.TabIndex = 6;
            this.progressBar1.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveSettingsToolStripMenuItem,
            this.loadSettingsToolStripMenuItem,
            this.deleteSettingsToolStripMenuItem,
            this.toolStripMenuItem4,
            this.helpToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(152, 98);
            // 
            // saveSettingsToolStripMenuItem
            // 
            this.saveSettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.toolStripMenuItem2});
            this.saveSettingsToolStripMenuItem.Name = "saveSettingsToolStripMenuItem";
            this.saveSettingsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.saveSettingsToolStripMenuItem.Text = "&Save settings";
            this.saveSettingsToolStripMenuItem.DropDownOpening += new System.EventHandler(this.saveSettingsToolStripMenuItem_DropDownOpening);
            this.saveSettingsToolStripMenuItem.Click += new System.EventHandler(this.saveSettingsToolStripMenuItem_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 23);
            this.toolStripTextBox1.Enter += new System.EventHandler(this.toolStripTextBox1_Enter);
            this.toolStripTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBox1_KeyPress);
            this.toolStripTextBox1.Click += new System.EventHandler(this.toolStripTextBox1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(157, 6);
            // 
            // loadSettingsToolStripMenuItem
            // 
            this.loadSettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.defaultsToolStripMenuItem1,
            this.lastSavedToolStripMenuItem,
            this.toolStripMenuItem1});
            this.loadSettingsToolStripMenuItem.Name = "loadSettingsToolStripMenuItem";
            this.loadSettingsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.loadSettingsToolStripMenuItem.Text = "&Load settings";
            this.loadSettingsToolStripMenuItem.DropDownOpening += new System.EventHandler(this.loadSettingsToolStripMenuItem_DropDownOpening);
            this.loadSettingsToolStripMenuItem.Click += new System.EventHandler(this.loadSettingsToolStripMenuItem_Click);
            // 
            // defaultsToolStripMenuItem1
            // 
            this.defaultsToolStripMenuItem1.Name = "defaultsToolStripMenuItem1";
            this.defaultsToolStripMenuItem1.Size = new System.Drawing.Size(128, 22);
            this.defaultsToolStripMenuItem1.Text = "&Defaults";
            this.defaultsToolStripMenuItem1.Click += new System.EventHandler(this.defaultsToolStripMenuItem1_Click);
            // 
            // lastSavedToolStripMenuItem
            // 
            this.lastSavedToolStripMenuItem.Name = "lastSavedToolStripMenuItem";
            this.lastSavedToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.lastSavedToolStripMenuItem.Text = "&Last saved";
            this.lastSavedToolStripMenuItem.Click += new System.EventHandler(this.lastSavedToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(125, 6);
            // 
            // deleteSettingsToolStripMenuItem
            // 
            this.deleteSettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3});
            this.deleteSettingsToolStripMenuItem.Name = "deleteSettingsToolStripMenuItem";
            this.deleteSettingsToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.deleteSettingsToolStripMenuItem.Text = "&Delete settings";
            this.deleteSettingsToolStripMenuItem.DropDownOpening += new System.EventHandler(this.deleteSettingsToolStripMenuItem_DropDownOpening);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem3.Text = "placeholder";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(148, 6);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usingFileNameMacrosToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // usingFileNameMacrosToolStripMenuItem
            // 
            this.usingFileNameMacrosToolStripMenuItem.Name = "usingFileNameMacrosToolStripMenuItem";
            this.usingFileNameMacrosToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.usingFileNameMacrosToolStripMenuItem.Text = "&Using file name macros...";
            this.usingFileNameMacrosToolStripMenuItem.Click += new System.EventHandler(this.usingFileNameMacrosToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(811, 478);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox _lstTransformations;
        private System.Windows.Forms.Button _btnProcess;
        private System.Windows.Forms.PropertyGrid _propertyGrid;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button _btnMenu;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem saveSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripMenuItem defaultsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem deleteSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem lastSavedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usingFileNameMacrosToolStripMenuItem;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

