﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat
{
    enum FolderType
    {
        [FolderNameAttribute("NMR")]
        Nmr,

        [FolderNameAttribute("Batch")]
        Batch,

        [FolderNameAttribute("GP")]
        Gp,

        [FolderNameAttribute("Manipulation")]
        Manip,

        [FolderNameAttribute("Miscellaneous")]
        Misc,

        [FolderNameAttribute("Boids")]
        Boids,
    }

    class FolderAttribute : Attribute
    {
        public FolderAttribute(FolderType ft)
        {
            Folder = ft;
        }

        public FolderType Folder { get; set; }
    }

    class FolderNameAttribute : Attribute
    {
        public FolderNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
