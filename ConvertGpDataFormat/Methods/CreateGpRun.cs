﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    class NotIncludedAttribute : Attribute
    {
    }

    class AllowFilenameMacrosAttribute : Attribute
    {
    }

    [DisplayName("Create GP script file")]
    [Description("Creates and optionally runs a GP script file")]
    [Folder(FolderType.Gp)]
    [Serializable]
    class CreateGpRun : Transformation
    {
        public enum ProcessingMode
        {
            train,
            evaluate
        }

        public enum FitnessFunction
        {
            classifier,
            difference
        }

        public enum SelectionMethod
        {
            rank,
            fitness
        }

        [Description("An arbitrary comment put into the output files.")]
        [Category("1. Miscellaneous")]
        [DefaultValue("")]
        [DisplayName("Description")]
        public string description { get; set; }

        [Description("What feature of this application is used.")]
        [Category("1. Miscellaneous")]
        [DefaultValue(ProcessingMode.train)]
        [DisplayName("Mode")]
        public ProcessingMode mode { get; set; }

        [Description("This is the index of the test and the name given to the output folder.")]
        [Category("1. Miscellaneous")]
        [DefaultValue(0)]
        [DisplayName("Starting iteration number")]
        public int iteration { get; set; }

        [Description("Multiple training runs may be performed by setting this value. Each run automatically increments the iteration variable.")]
        [Category("1. Miscellaneous")]
        [DefaultValue(100)]
        [DisplayName("Iterations")]
        public int iterations { get; set; }

        [Description("What fraction of the top performing programs to list the common variables for")]
        [Category("1. Miscellaneous")]
        [DefaultValue(0.1d)]
        [DisplayName("Common variable fraction")]
        public double common_variable_listing { get; set; }

        [Description("Minimum value of constants generated")]
        [Category("1. Miscellaneous")]
        [DefaultValue(-1000d)]
        [DisplayName("Constant minimum value")]
        public double constant_minimum { get; set; }

        [Description("Maximum value of constants generated")]
        [Category("1. Miscellaneous")]
        [DefaultValue(1000d)]
        [DisplayName("Constant maximum value")]
        public double constant_maximum { get; set; }

        [Description("Number of programs in the population")]
        [Category("2. Population")]
        [DefaultValue(200)]
        [DisplayName("Population size")]
        public int population_size { get; set; }

        [Description("Maximum depth the program trees are allowed to grow to")]
        [Category("2. Population")]
        [DefaultValue(8)]
        [DisplayName("Maximum tree depth")]
        public int tree_depth { get; set; }

        [Description("How fitness for each program is calculated. See the enum for details.")]
        [Category("2. Population")]
        [DefaultValue(FitnessFunction.classifier)]
        [DisplayName("Fitness function")]
        public FitnessFunction fitness_function { get; set; }

        [Description("How programs are selected for reproduction. See the enum for details.")]
        [Category("2. Population")]
        [DefaultValue(SelectionMethod.rank)]
        [DisplayName("Selection method")]
        public SelectionMethod selection_method { get; set; }

        [Description("Operators available to the programs in the population")]
        [Category("2. Population")]
        [DefaultValue("ALL")]
        [DisplayName("Operators")]
        public string operators { get; set; }

        [Description("The fraction of individuals to generate each generation by REPLICATION")]
        [Category("3. Replication")]
        [DefaultValue(0.1d)]
        [DisplayName("Replication")]
        public double replication_breeding { get; set; }

        [Description("The fraction of individuals to generate each generation by CROSSOVER")]
        [Category("3. Replication")]
        [DefaultValue(0.75d)]
        [DisplayName("Crossover")]
        public double crossover_breeding { get; set; }

        [Description("The fraction of individuals to generate each generation by MUTATION")]
        [Category("3. Replication")]
        [DefaultValue(0.15d)]
        [DisplayName("Mutation")]
        public double mutation_breeding { get; set; }

        [Description("The number of worst-fitting programs in each generation to always discard")]
        [Category("3. Replication")]
        [DefaultValue(0.1d)]
        [DisplayName("Discard weakest")]
        public double discard_weakest { get; set; }

        [Description("The chance of individual programs being mutated AFTER insertion into new population")]
        [Category("3. Replication")]
        [DefaultValue(1.0d)]
        [DisplayName("Mutation aftereffect")]
        public double mutation_aftereffect { get; set; }

        [Description("How many populations are run in parallel")]
        [Category("4. Islands")]
        [DefaultValue(5)]
        [DisplayName("Islands")]
        public int number_of_populations { get; set; }

        [Description("How often (in generations) migration occurs from one population to adjacent populations")]
        [Category("4. Islands")]
        [DefaultValue(50)]
        [DisplayName("Migration interval")]
        public int migration_interval { get; set; }

        [Description("The number of programs to copy to adjacent populations when migration occurs")]
        [Category("4. Islands")]
        [DefaultValue(0.05d)]
        [DisplayName("Migration quantity")]
        public double migration_quantity { get; set; }

        [Description("Training will stop after this many generations.")]
        [Category("5. Stopping conditions")]
        [DefaultValue(1000)]
        [DisplayName("Generations")]
        public int stop_after_generations { get; set; }

        [Description("Training will stop when fitness reaches this value.\r\nFor classifier fitness function this is when the fraction of results CORRECT goes above this value.\r\nFor difference fitness this is when the average distance to the correct result goes below this value.\r\n\r\nTraining will stop when fitness reaches this value.\r\nFor classifier fitness function this is when the fraction of results CORRECT goes above this value.")]
        [Category("5. Stopping conditions")]
        [DefaultValue(1.0d)]
        [DisplayName("Matches")]
        public double stop_after_matches { get; set; }

        [Description("Training will stop if fitness sees no improvement over this many generations.")]
        [Category("5. Stopping conditions")]
        [DefaultValue(50)]
        [DisplayName("Unchanging")]
        public int stop_after_unchanging { get; set; }

        [Description("This has the same semantics as stop_after_matches.\r\nWhen this stopping condition is reached training will RESTART using the previously selected variables.")]
        [Category("6. Two-stage")]
        [DefaultValue(0d)]
        [DisplayName("Matches")]
        public double two_stage_matches { get; set; }

        [Description("This is how many neighbours of the two stage variables to choose within (1 means 1 to either side, i.e. a total range of 3)")]
        [Category("6. Two-stage")]
        [DefaultValue(0)]
        [DisplayName("Variable margin")]
        public int two_stage_variable_range { get; set; }

        [Description("Data sets")]
        [Category("7. I/O")]
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        [DisplayName("Training data")]
        [AllowFilenameMacros]
        public string data { get; set; }

        [Description("Data sets, used in validation only ")]
        [Category("7. I/O")]
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        [DisplayName("Validation data")]
        [AllowFilenameMacros]
        public string validation_data { get; set; }

        [Description("Where the output files are placed")]
        [Category("7. I/O")]
        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        [DisplayName("Results directory")]
        [AllowFilenameMacros]
        public string output_directory { get; set; }

        [Description("GP Program to run. If this is EMPTY training will be performed and a new program will be determined rather than using an existing one.")]
        [Category("8. Playback")]
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        [DisplayName("Playback program")]
        [AllowFilenameMacros]
        public string program_file { get; set; }

        [Description("Where to save the created script file.")]
        [Category("0. Main")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        [NotIncluded]
        [DisplayName("Created script file")]
        public string ScriptFile { get; set; }

        [Description("Whether to append to the file or create a new file")]
        [Category("0. Main")]
        [DefaultValue(false)]
        [NotIncluded]
        [DisplayName("Append to script file")]
        public bool Append { get; set; }

        [Description("If Append is false or if the file does not already exist this will include a pause.")]
        [Category("0. Main")]
        [DefaultValue(true)]
        [NotIncluded]
        [DisplayName("Include pause in script file")]
        public bool IncludePause { get; set; }

        [Description("If GpExe and BatchFile are set then a batch file to run the GP exe is created. This may be optionally be run with by controlling the Execute.")]
        [Category("0. Main")]
        [DefaultValue(false)]
        [NotIncluded]
        [DisplayName("Execute batch file")]
        public bool Execute { get; set; }

        [Description("If this and BatchFile are set then a batch file to run the GP exe is created. This may be optionally be run with the Execute flag.")]
        [Category("0. Main")]
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        [NotIncluded]
        [DisplayName("Location of GP.exe")]
        [AllowFilenameMacros]
        public string GpExe { get; set; }

        [Description("If GpExe and this are set then a batch file to run the GP exe is created. This may be optionally be run with the Execute flag.")]
        [Category("0. Main")]
        [DefaultValue("{PATH}{FILE}.bat")]
        [NotIncluded]
        [DisplayName("Created batch file")]
        [AllowFilenameMacros]
        public string BatchFile { get; set; }

        public CreateGpRun()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            bool append = File.Exists(ScriptFile) & Append;

            using (StreamWriter sw = new StreamWriter(ScriptFile, append))
            {
                PropertyInfo[] properties = this.GetType().GetProperties();

                foreach (var property in properties)
                {
                    NotIncludedAttribute attr = property.GetCustomAttribute<NotIncludedAttribute>(true);
                    string name = property.Name;

                    if (attr != null)
                    {
                        name = "# " + name;
                    }

                    string value = property.GetValue(this).ToString();

                    if (property.PropertyType == typeof(string))
                    {
                        if (property.GetCustomAttribute<AllowFilenameMacrosAttribute>() != null)
                        {
                            value = base.Ffn(value, ScriptFile);
                        }

                        value = "\"" + value + "\"";
                    }

                    sw.WriteLine(name.PadRight(31) + " " + value);

                }

                if (!append && IncludePause)
                {
                    sw.WriteLine();
                    sw.WriteLine("echo \"Press any key to begin test.\"");
                    sw.WriteLine("pause");
                }

                sw.WriteLine();
                sw.WriteLine("process");
            }

            rp.AddOutput("Script", ScriptFile);

            string batchFile = base.Ffn(BatchFile, ScriptFile);

            if (!string.IsNullOrWhiteSpace(batchFile) && !string.IsNullOrWhiteSpace(GpExe))
            {
                using (StreamWriter sw = new StreamWriter(batchFile))
                {
                    sw.WriteLine("\"" + base.Ffn(GpExe, ScriptFile) + "\" " + Path.GetFileName(ScriptFile));
                    sw.WriteLine("pause");

                    rp.AddOutput("Batch", batchFile);
                }

                if (Execute)
                {
                    ProcessStartInfo psi = new ProcessStartInfo(batchFile);
                    psi.WorkingDirectory = Path.GetDirectoryName(ScriptFile);
                    System.Diagnostics.Process.Start(psi);

                    rp.AddOutput("Executed", batchFile);
                }
            }
        }
    }
}
