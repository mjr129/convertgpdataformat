﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Convert between GP formats")]
    [Serializable]
    [Folder(FolderType.Gp)]
    class GpFormatConversion : Transformation
    {
        public enum InFileFormat
        {
            Mjr,
            Rad
        }

        public enum OutFileFormat
        {
            Mjr,
            Rad,
            Data,
            Classes,
            Indexes,
        }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File containing data (any format, one observation per line)")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file")]
        [DefaultValue("")]
        public string OutputFile { get; set; }

        [Category("Outputs")]
        [Description("Format of created file.")]
        [DefaultValue(InFileFormat.Mjr)]
        public InFileFormat InputFormat { get; set; }

        [Category("Outputs")]
        [Description("Format of created file.")]
        [DefaultValue(OutFileFormat.Rad)]
        public OutFileFormat OutputFormat { get; set; }

        public GpFormatConversion()
        {
            InputFile = "";
            OutputFile = "";
            InputFormat = InFileFormat.Mjr;
            OutputFormat = OutFileFormat.Rad;
        }

        public override void Run(ResultsPackage rp)
        {
            string outfile = base.Ffn(OutputFile, InputFile);

            using (StreamReader d = new StreamReader(InputFile))
            {
                int lineIndex = -1;

                using (StreamWriter w = new StreamWriter(outfile))
                {
                    string line = d.ReadLine();
                    lineIndex++;

                    string data;
                    string index;
                    string @class;

                    int endOfFirstColumn = line.IndexOfAny(" \t,".ToCharArray());

                    switch (InputFormat)
                    {
                        case InFileFormat.Mjr:
                            @class = line.Substring(0, endOfFirstColumn);
                            index = lineIndex.ToString();
                            data = line.Substring(endOfFirstColumn);
                            break;

                        case InFileFormat.Rad:
                            index = line.Substring(0, endOfFirstColumn);
                            int startOfLastColumn = line.LastIndexOfAny(" \t,".ToCharArray());
                            @class = line.Substring(startOfLastColumn);
                            data = line.Substring(endOfFirstColumn, line.Length - endOfFirstColumn - startOfLastColumn);
                            break;

                        default:
                            throw new InvalidEnumArgumentException("InputFormat", (int)InputFormat, typeof(InFileFormat));
                    }

                    switch (OutputFormat)
                    {
                        case OutFileFormat.Mjr:
                            w.Write(@class);
                            w.Write("\t");
                            w.WriteLine(data);
                            break;

                        case OutFileFormat.Rad:
                            w.Write(index);
                            w.Write("\t");
                            w.WriteLine(data);
                            w.Write("\t");
                            break;

                        case OutFileFormat.Classes:
                            w.WriteLine(@class);
                            break;

                        case OutFileFormat.Data:
                            w.WriteLine(data);
                            break;

                        case OutFileFormat.Indexes:
                            w.WriteLine(index);
                            break;

                        default:
                            throw new InvalidEnumArgumentException("OutputFormat", (int)OutputFormat, typeof(OutFileFormat));
                    }
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
