﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    class ProcessStreamsArgs
    {
        public readonly ResultsPackage Meta;
        public readonly char[] CsvSeparatorsIn;
        public readonly StringSplitOptions StringSplitOptions;
        public readonly StreamReader Input;
        public readonly StreamWriter Output;
        public readonly string CsvSeparatorOut;

        public ProcessStreamsArgs(ResultsPackage rp, char[] csvSeparatorsIn, StringSplitOptions stringSplitOptions, StreamReader sr, StreamWriter sw, string csvSeparatorOut)
        {
            this.Meta = rp;
            this.CsvSeparatorsIn = csvSeparatorsIn;
            this.StringSplitOptions = stringSplitOptions;
            this.Input = sr;
            this.Output = sw;
            this.CsvSeparatorOut = csvSeparatorOut;
        }
    }

    class ProcessLineArgs
    {
        public readonly string Line;
        public readonly string[] Fields;
        public readonly StreamWriter Output;
        public readonly ResultsPackage Meta;
        public readonly string CsvSeparatorOut;
        public readonly int LineNumber;

        public ProcessLineArgs(string l, string[] e, string csvSeparatorOut, StreamWriter sw, ResultsPackage rp, int lineNumber)
        {
            this.Line = l;
            this.Fields = e;
            this.Output = sw;
            this.Meta = rp;
            this.CsvSeparatorOut = csvSeparatorOut;
            this.LineNumber = lineNumber;
        }
    }

    class RunArgs
    {
        public readonly ResultsPackage Meta;
        public readonly char[] CsvSeparatorsIn;
        public readonly string CsvSeparatorOut;
        public readonly string OutputFile;
        public readonly StringSplitOptions StringSplitOptions;
        public readonly string InputFile;

        public RunArgs(ResultsPackage rp, char[] csvSeparatorsIn, string csvSeparatorOut, string outputFile, StringSplitOptions stringSplitOptions, string InputFile)
        {
            this.Meta = rp;
            this.CsvSeparatorsIn = csvSeparatorsIn;
            this.CsvSeparatorOut = csvSeparatorOut;
            this.OutputFile = outputFile;
            this.StringSplitOptions = stringSplitOptions;
            this.InputFile = InputFile;
        }
    }

    [Serializable]
    abstract class CsvInOutTransformation : Transformation
    {
        protected CsvInOutTransformation()
        {
            base.ApplyDefaultsFromAttributes();
        }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Filename")]
        [Category("Inputs")]
        [Description("File to read")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Filename")]
        [Category("Outputs")]
        [Description("File to create")]
        [DefaultValue("{PATH}{FILE}-modified{EXT}")]
        public string OutputFile { get; set; }

        [Description("CSV separator for output file")]
        [DisplayName("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string OutputCsvSeparator { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [DisplayName("CSV separator(s)")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("Ignore empty cells (use if data has been justified by adding spaces, etc.).")]
        [DisplayName("Ignore empty columns")]
        [Category("Inputs")]
        [DefaultValue(false)]
        public bool RemoveEmpties { get; set; }

        // Override this to use as normal
        public override void Run(ResultsPackage rp)
        {
            char[] csvSeparatorsIn = base.Escape(InputCsvSeparator).ToCharArray();
            string csvSeparatorOut = base.Escape(OutputCsvSeparator);
            string outputFile = base.Ffn(OutputFile, InputFile);
            StringSplitOptions stringSplitOptions = RemoveEmpties ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            ProcessFiles(new RunArgs(rp, csvSeparatorsIn, csvSeparatorOut, outputFile, stringSplitOptions, InputFile));

            rp.Add("Input file", InputFile, ResultsType.FileName);
            rp.AddOutput(outputFile);
        }

        // Override this if you want the filenames
        protected virtual void ProcessFiles(RunArgs e)
        {
            using (StreamReader sr = new StreamReader(e.InputFile))
            {
                using (StreamWriter sw = new StreamWriter(e.OutputFile))
                {
                    ProcessStreams(new ProcessStreamsArgs(e.Meta, e.CsvSeparatorsIn, e.StringSplitOptions, sr, sw, e.CsvSeparatorOut));
                }
            }
        }

        // Override this to process the streams
        protected virtual void ProcessStreams(ProcessStreamsArgs e)
        {
            int linesInInput = 0;

            while (!e.Input.EndOfStream)
            {
                e.Meta.ReportProgress(e.Input);

                linesInInput++;

                var f = ReadLine(e, linesInInput);

                ProcessLine(f);
            }

            e.Meta.Add("Lines in input", linesInInput.ToString(), ResultsType.Information);
        }

        // Override this to process the lines
        protected virtual void ProcessLine(ProcessLineArgs e)
        {
            WriteLine(e.Output, e.Fields, e.CsvSeparatorOut);
        }

        /// <summary>
        /// Call this to process an input CSV line
        /// </summary>
        protected ProcessLineArgs ReadLine(ProcessStreamsArgs e, int lineNumber)
        {
            string l = e.Input.ReadLine();
            string[] fields = l.Split(e.CsvSeparatorsIn, e.StringSplitOptions);
            var r = new ProcessLineArgs(l, fields, e.CsvSeparatorOut, e.Output, e.Meta, lineNumber);
            return r;
        }

        /// <summary>
        /// Call this to process an input CSV line
        /// </summary>
        protected ProcessLineArgs ReadLine(ProcessStreamsArgs e)
        {
            return ReadLine(e, -1);
        }

        /// <summary>
        /// Call this to process an input CSV line
        /// </summary>
        protected string[] ReadLine(RunArgs e, StreamReader sr)
        {
            return ReadLine(sr, e.CsvSeparatorsIn, e.StringSplitOptions);
        }

        /// <summary>
        /// Call this to process an input CSV line
        /// </summary>
        protected string[] ReadLine(StreamReader sr, char[] separators, StringSplitOptions sso)
        {
            string l = sr.ReadLine();

            if (l.Length == 0)
            {
                return null;
            }

            string[] fields = l.Split(separators, sso);
            return fields;
        }

        /// <summary>
        /// Call this to write a CSV line.
        /// </summary>
        protected static void WriteLine(ProcessStreamsArgs e, IEnumerable fields, string rowName = null)
        {
            WriteLine(e.Output, fields, e.CsvSeparatorOut, rowName);
        }

        /// <summary>
        /// Call this to write a CSV line.
        /// </summary>
        protected static void WriteLine(ProcessLineArgs e, IEnumerable fields, string rowName = null)
        {
            WriteLine(e.Output, fields, e.CsvSeparatorOut, rowName);
        }

        /// <summary>
        /// Call this to write a CSV line.
        /// </summary>
        protected static void WriteLine(StreamWriter output, IEnumerable fields, string separator, string rowName = null)
        {
            bool firstField = true;

            if (rowName != null)
            {
                output.Write(rowName);
                firstField = false;
            }

            foreach (object field in fields)
            {
                if (firstField)
                {
                    firstField = false;
                }
                else
                {
                    output.Write(separator);
                }

                output.Write(field.ToString());
            }

            output.WriteLine();
        }
    }
}
