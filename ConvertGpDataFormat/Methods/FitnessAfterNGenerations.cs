﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Fitness after n generations")]
    [Serializable]
    [Folder(FolderType.Gp)]
    class FitnessAfterNGenerations : Transformation
    {
        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Directory will be recursivley searched for best_program.csv files.")]
        [DefaultValue("")]
        public string InputDirectory { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file, macros are accepted.")]
        [DefaultValue("{PATH}FitnessAfter{N}Generations.csv")]
        public string OutputFile { get; set; }

        [Category("Inputs")]
        [DefaultValue(100)]
        public int Generation { get; set; }

        public FitnessAfterNGenerations()
        {
            Generation = 100;
            InputDirectory = "";
            OutputFile = "{PATH}FitnessAfter{N}Generations.csv";
        }

        public override void Run(ResultsPackage rp)
        {
            string[] files = Directory.GetFiles(InputDirectory, "generations.csv", SearchOption.AllDirectories);
            string outputFile = FfnDir(OutputFile, InputDirectory);
            outputFile = outputFile.Replace("{N}", Generation.ToString());
            List<decimal> list = new List<decimal>();

            foreach (string file in files)
            {
                foreach (string line in File.ReadAllLines(file))
                {
                    string[] elements = line.Split(",".ToCharArray());

                    if (elements.Length == 4)
                    {
                        if (elements[1].Trim() == Generation.ToString())
                        {
                            list.Add(decimal.Parse(elements[2]));
                            break;
                        }
                    }
                }
            }

            list.Sort();

            using (StreamWriter sw = new StreamWriter(outputFile))
            {
                foreach (decimal d in list)
                {
                    sw.WriteLine(d);
                }
            }

            rp.Add("Files read", files.Length.ToString(), ResultsType.Information);
            rp.AddOutput(outputFile);
        }
    }
}
