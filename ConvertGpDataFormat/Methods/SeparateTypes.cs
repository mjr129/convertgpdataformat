﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Determine classes from CSV/TSV file")]
    [Description("Creates a list of classes from an input file.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class SeparateTypes : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Input file (CSV format)")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Inputs")]
        [Description("Input file delimiters")]
        [DefaultValue(" \t,")]
        public string InputFileDelimiters { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file (TSV format - one class per line)")]
        [DefaultValue("")]
        public string OutputFile { get; set; }

        [Category("Inputs")]
        [Description("Index of column to use for class determination.")]
        [DefaultValue(7)]
        public int ColumnIndex { get; set; }

        [Category("Inputs")]
        [Description("Value of specified column to indicate class 1 (otherwise output is class 0). Not case sentitive. If this is empty the column itself will be output.")]
        [DefaultValue("CORSICA")]
        public string Pattern { get; set; }

        [Category("Inputs")]
        [Description("Whether the input file contains headers.")]
        [DefaultValue(false)]
        public bool Headers { get; set; }

        public SeparateTypes()
        {
            InputFile = "";
            InputFileDelimiters = " \t,";
            OutputFile = "";
            ColumnIndex = 7;
            Pattern = "CORSICA";
            Headers = false;
        }

        public override void Run(ResultsPackage rp)
        {
            string pattern = (Pattern != null) ? Pattern.ToUpper() : "";
            string outfile = base.Ffn(OutputFile, InputFile);

            using (StreamReader sr = new StreamReader(InputFile))
            {
                using (StreamWriter sw = new StreamWriter(outfile))
                {
                    if (Headers)
                    {
                        sr.ReadLine(); // headers
                    }

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        string[] elems = line.Split(InputFileDelimiters.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        if (elems.Length != 0)
                        {
                            if (!string.IsNullOrEmpty(Pattern))
                            {
                                if (elems[ColumnIndex].ToUpper() == pattern)
                                {
                                    sw.WriteLine("1");
                                }
                                else
                                {
                                    sw.WriteLine("0");
                                }
                            }
                            else
                            {
                                sw.WriteLine(elems[ColumnIndex]);
                            }
                        }
                    }
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
