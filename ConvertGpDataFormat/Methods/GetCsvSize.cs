﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Get CSV size")]
    [Description("Gets size of CSV file")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class GetCsvSize : CsvInOutTransformation
    {
        protected override void ProcessStreams(ProcessStreamsArgs e)
        {
            int cols = 0;
            int rows = 0;

            while (!e.Input.EndOfStream)
            {
                e.Meta.ReportProgress(e.Input);

                var l = ReadLine(e);

                if (l.Line.Length != 0)
                {
                    rows++;

                    if (cols == 0)
                    {
                        cols = l.Fields.Length;
                    }
                    else
                    {
                        if (cols != l.Fields.Length)
                        {
                            throw new FormatException("Not all lines are the same size, line " + (rows) + " has " + l.Fields.Length + " fields but the line(s) before it have " + cols + " fields.");
                        }
                    }
                }
            }

            e.Output.WriteLine("NumCols" + e.CsvSeparatorOut + cols);
            e.Output.WriteLine("NumRows" + e.CsvSeparatorOut + rows);
            e.Output.WriteLine("NumVals" + e.CsvSeparatorOut + cols * rows);
        }
    }
}
