﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Concatenate data")]
    [Description("Concatenates the lines from two input files\r\n<value><data> = <value>,<data1>,<data2>\r\nThe <value> fields must match in both data files.\r\nThe output file will contain the number of lines found in the shortest file.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class ConcatenateData : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the left side of the lines are taken from")]
        [DefaultValue("")]
        public string LeftInputFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the right side of the lines are taken from")]
        [DefaultValue("")]
        public string RightInputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH1}{FILE1}+{FILE2}{EXT1}")]
        public string OutputFile { get; set; }

        [Description("CSV separator")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string CsvSeparator { get; set; }

        public ConcatenateData()
        {
            OutputFile = "{PATH1}{FILE1}+{FILE2}{EXT1}";
            CsvSeparator = ",";
            InputCsvSeparator = "{TAB}{SPACE},";
        }

        public override void Run(ResultsPackage rp)
        {
            string outFileName = Ffn(OutputFile, LeftInputFile, RightInputFile);
            string inCsvSep = Escape(InputCsvSeparator);
            string csvSep = Escape(CsvSeparator);
            int carried = 0;
            int dropped = 0;
            int truncated = 0;

            using (StreamReader left = new StreamReader(LeftInputFile))
            {
                using (StreamReader right = new StreamReader(RightInputFile))
                {
                    using (StreamWriter output = new StreamWriter(outFileName))
                    {
                        while (!left.EndOfStream && !right.EndOfStream)
                        {
                            string l1 = left.ReadLine();
                            string l2 = right.ReadLine();

                            string[] e1 = l1.Split(inCsvSep.ToCharArray(), 2);
                            string[] e2 = l2.Split(inCsvSep.ToCharArray(), 2);

                            if (e1[0] == e2[0])
                            {
                                output.Write(e1[0]);
                                output.Write(csvSep);
                                output.Write(e1[1]);
                                output.Write(csvSep);
                                output.WriteLine(e2[1]);
                                carried++;
                            }
                            else
                            {
                                dropped++;
                            }
                        }

                        while (!left.EndOfStream)
                        {
                            left.ReadLine();
                            truncated--;
                        }

                        if (!right.EndOfStream)
                        {
                            System.Diagnostics.Debug.Assert(truncated == 0);

                            while (!right.EndOfStream)
                            {
                                right.ReadLine();
                                truncated++;
                            }
                        }
                    }
                }
            }

            rp.AddInfo("Carried", carried);
            rp.AddInfo("Dropped", dropped);
            rp.AddInfo("Truncated (L-/R+)", truncated);
            rp.AddOutput(outFileName);
        }
    }
}
