﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("TSV <--> CSV")]
    [Description("Convert TSV to CSV and vice versa")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class TsvToCsv : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File to read in")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH}{FILE}-converted.csv")]
        public string OutputFile { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string OutputCsvSeparator { get; set; }

        [Description("Ignore empty cells (use if data has been justified by adding spaces, etc.).")]
        [Category("Inputs")]
        [DefaultValue(false)]
        public bool MergeFields { get; set; }

        public TsvToCsv()
        {
            OutputFile = "{PATH}{FILE}-converted.csv";
            InputCsvSeparator = "{TAB}{SPACE},";
            OutputCsvSeparator = ",";
            MergeFields = false;
        }

        public override void Run(ResultsPackage rp)
        {
            char[] icsv = base.Escape(InputCsvSeparator).ToCharArray();
            string ocsv = base.Escape(OutputCsvSeparator);
            string outfile = base.Ffn(OutputFile, InputFile);
            StringSplitOptions sso = MergeFields ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            using (StreamReader sr = new StreamReader(InputFile))
            {
                using (StreamWriter sw = new StreamWriter(outfile))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] elements = sr.ReadLine().Split(icsv, sso);

                        for (int n = 0; n < elements.Length; n++)
                        {
                            if (n != 0)
                            {
                                sw.Write(OutputCsvSeparator);
                            }

                            sw.Write(elements[n]);
                        }

                        sw.WriteLine();
                    }
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
