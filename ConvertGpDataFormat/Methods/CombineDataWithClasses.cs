﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Create GP data file")]
    [Description("Merges a data file with a list of classes.")]
    [Folder(FolderType.Gp)]
    [Serializable]
    class CombineDataWithClasses : Transformation
    {
        public enum FileFormat
        {
            [Description("My format - (class) (data)")]
            Mjr,
            [Description("R. A. Davis's format - (index) (data) (class)")]
            Rad,
        }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File containing data (any format, one observation per line)")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Inputs")]
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("File containing classes (one class per line, each line matching the same line of the InputFile)")]
        [DefaultValue("")]
        public string ClassFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file. Macros are accepted (Input = 1, Class = 2).")]
        [DefaultValue("{PATH1}{FILE1}.gpin{EXT1}")]
        public string OutputFile { get; set; }

        [Category("Outputs")]
        [Description("Format of created file.")]
        [DefaultValue(FileFormat.Mjr)]
        public FileFormat GpFormat { get; set; }

        [Description("CSV separators if only using a single column from the input class file. Any and all of these will be accepted.")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("Set to a non-negative value to only use one column from the class file. Column offsets start from 0.")]
        [Category("Inputs")]
        [DefaultValue(-1)]
        public int InputColumn { get; set; }

        [Description("Skip this many initial lines in the class file")]
        [Category("Inputs")]
        [DefaultValue(0)]
        public int ClassHeaders { get; set; }

        [Description("Skip this many initial lines in the data file")]
        [Category("Inputs")]
        [DefaultValue(0)]
        public int DataHeaders { get; set; }

        [Description("Merge empty cells when columns are being counted in the class file (use if data has been justified by adding spaces, etc.).")]
        [Category("Inputs")]
        [DefaultValue(false)]
        public bool MergeFields { get; set; }

        [Description("CSV separator (Mjr format only). Note that data nor classes will not be reformatted, this is just the delimiter placed the class and data. For Davies's format this is always a space.")]
        [Category("Outputs")]
        [DefaultValue("{TAB}")]
        public string CsvSeparator { get; set; }

        public CombineDataWithClasses()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            string outputFileName = Ffn(OutputFile, InputFile, ClassFile);
            char[] icsvSep = Escape(InputCsvSeparator).ToCharArray();
            string csvSep = Escape(CsvSeparator);
            StringSplitOptions sso = MergeFields ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            using (StreamReader d = new StreamReader(InputFile))
            {
                using (StreamReader c = new StreamReader(ClassFile))
                {
                    using (StreamWriter w = new StreamWriter(outputFileName))
                    {
                        for (int h = 0; h < ClassHeaders; h++)
                        {
                            c.ReadLine();
                        }

                        for (int h = 0; h < DataHeaders; h++)
                        {
                            d.ReadLine();
                        }

                        int n = 0;
                        while (!(d.EndOfStream && c.EndOfStream))
                        {
                            if (d.EndOfStream)
                            {
                                throw new Exception("Data file ended before class file at entry " + n);
                            }

                            if (c.EndOfStream)
                            {
                                throw new Exception("Class file ended before data file at entry " + n);
                            }

                            string d1 = d.ReadLine();
                            string c1 = c.ReadLine();

                            if (!(string.IsNullOrWhiteSpace(d1) && string.IsNullOrWhiteSpace(c1)))
                            {
                                if (InputColumn >= 0)
                                {
                                    string[] e = c1.Split(icsvSep, sso);

                                    c1 = e[InputColumn];
                                }

                                if (GpFormat == FileFormat.Mjr)
                                {
                                    w.WriteLine(c1 + csvSep + d1);
                                }
                                else
                                {
                                    w.WriteLine(n.ToString() + " " + d1 + " " + c1);
                                }
                                n++;
                            }
                        }

                        Debug.Assert(c.EndOfStream);
                    }
                }
            }

            rp.AddOutput(outputFileName);
        }
    }
}
