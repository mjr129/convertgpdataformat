﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Split into training and validation data, optionally preserving class ratio")]
    [Description("Randomly splits the lines from the input file into two output files. Lines are split in a manner such that the percentage of data of a particular class in the output files matches the percentage found in the original file.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class ExtractRandom3 : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Input file")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("The file the classes are read from (this may be the same as the input file)")]
        [DefaultValue("{PATH}{FILE}{EXT}")]
        public string ClassFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("{PATH}{FILE}-training{EXT}")]
        public string TrainingOutputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("{PATH}{FILE}-validation{EXT}")]
        public string ValidationOutputFile { get; set; }

        [Category("Behaviour")]
        [Description("What fraction of the data goes into the validation file.")]
        [DefaultValue(0.25)]
        public double ValidationFraction { get; set; }

        [Category("Behaviour")]
        [Description("Which column contains the class, -1 for unclassed data or to not preserve class ratio")]
        [DefaultValue(-1)]
        public int ClassColumn { get; set; }

        [Category("Behaviour")]
        [Description("How many lines are headers?")]
        [DefaultValue(0)]
        public int ReadHeaders { get; set; }

        [Category("Behaviour")]
        [Description("How many lines are headers in the class file?")]
        [DefaultValue(0)]
        public int ReadClassHeaders { get; set; }

        [Category("Behaviour")]
        [Description("Whether to output any headers read in")]
        [DefaultValue(true)]
        public bool WriteHeaders { get; set; }

        [Category("Behaviour")]
        [Description("Variable numbers which are always forced into the validation set (e.g. outliers). Such variables are discounted when preserving class ratios.")]
        public List<int> ForceValidation { get; set; }

        [Category("Behaviour")]
        [Description("File listing of one per line variable numbers which are always forced into the validation set (e.g. outliers). Such variables are discounted when preserving class ratios.")]
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        public string ForceValidationFile { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [Category("Behaviour")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("Ignore empty cells (use if data has been justified by adding spaces, etc.).")]
        [Category("Behaviour")]
        [DefaultValue(false)]
        public bool MergeFields { get; set; }

        [Description("Randomiser seed, this is useful if you have two or more files you want to split (e.g. data and meta-data) in the same manner. This won't work iof the files are different lengths, etc.")]
        [Category("Behaviour")]
        [DefaultValue(0)]
        public int Seed { get; set; }

        public ExtractRandom3()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            char[] icsv = base.Escape(InputCsvSeparator).ToCharArray();
            string classfile = base.Ffn(ClassFile, InputFile);
            string outfile1 = base.Ffn(TrainingOutputFile, InputFile);
            string outfile2 = base.Ffn(ValidationOutputFile, InputFile);
            StringSplitOptions sso = MergeFields ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            string[] headers = new string[ReadHeaders];
            Dictionary<string, List<string>> all = new Dictionary<string, List<string>>();
            List<string> forced = new List<string>();

            List<int> forcedIds = new List<int>(ForceValidation ?? new List<int>());

            if (!string.IsNullOrWhiteSpace(ForceValidationFile))
            {
                foreach (string fvl in File.ReadAllLines(ForceValidationFile))
                {
                    forcedIds.Add(int.Parse(fvl));
                }
            }

            using (StreamReader sr = new StreamReader(InputFile))
            {
                using (StreamReader src = new StreamReader(classfile))
                {
                    for (int n = 0; n < ReadHeaders; n++)
                    {
                        headers[n] = sr.ReadLine();
                    }

                    for (int n = 0; n < ReadClassHeaders; n++)
                    {
                        src.ReadLine();
                    }

                    int index = 0;

                    while (!sr.EndOfStream)
                    {
                        string l = sr.ReadLine();
                        string lc = src.ReadLine();

                        if (forcedIds.Contains(index))
                        {
                            forced.Add(l);
                        }
                        else
                        {
                            string c = "";

                            if (ClassColumn >= 0)
                            {
                                string[] e = lc.Split(icsv, sso);
                                c = e[ClassColumn];
                            }

                            List<string> list;

                            if (!all.TryGetValue(c, out list))
                            {
                                list = new List<string>();
                                all.Add(c, list);
                            }

                            list.Add(l);
                        }

                        index++;
                    }
                }
            }

            Random rnd = new Random(Seed);

            StringBuilder results = new StringBuilder();

            using (StreamWriter sw1 = base.SaveFile(outfile1))
            {
                using (StreamWriter sw2 = base.SaveFile(outfile2))
                {
                    if (WriteHeaders && headers.Length != 0)
                    {
                        rp.AddInfo("Headers", headers.Length);
                        string h = string.Join("\r\n", headers);
                        sw1.WriteLine(h);
                        sw2.WriteLine(h);
                    }

                    if (forced.Count != 0)
                    {
                        rp.AddInfo("Forced", forced.Count);
                        sw2.WriteLine(string.Join("\r\n", forced.ToArray()));
                    }

                    foreach (var kvp in all)
                    {
                        var list1 = kvp.Value;
                        var list2 = new List<string>();

                        int toMove = (int)(list1.Count * ValidationFraction + 0.5d);

                        for (int n = 0; n < toMove; n++)
                        {
                            int random = rnd.Next(list1.Count);
                            list2.Add(list1[random]);
                            list1.RemoveAt(random);
                        }

                        rp.AddInfo("Class (" + kvp.Key + ")", list1.Count + " + " + list2.Count + " = " + (list1.Count + list2.Count));

                        foreach (string i1 in list1)
                        {
                            sw1.WriteLine(i1);
                        }

                        foreach (string i2 in list2)
                        {
                            sw2.WriteLine(i2);
                        }
                    }
                }
            }

            rp.Add("Output 1", outfile1, ResultsType.FileName);
            rp.Add("Output 2", outfile2, ResultsType.FileName);
        }
    }
}
