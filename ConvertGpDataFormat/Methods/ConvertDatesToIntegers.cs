﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Convert dates to integers")]
    [Description("Convert dates of the form MM00YY to integers, with 1 being the first month.")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class ConvertDatesToIntegers : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File to read in")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("File to write out")]
        [DefaultValue("")]
        public string OutputFile { get; set; }

        [Category("Inputs")]
        [Description("Column to alter")]
        [DefaultValue(0)]
        public int Column { get; set; }

        public ConvertDatesToIntegers()
        {
            InputFile = "";
            OutputFile = "";
            Column = 0;
        }

        class Date
        {
            public int Year;
            public int Month;
            public string[] Elements;

            public Date(int year, int month, string[] elements)
            {
                this.Year = year;
                this.Month = month;
                this.Elements = elements;
            }

            public bool IsLower(Date b)
            {
                if (Year < b.Year)
                {
                    return true;
                }
                else if (Year == b.Year && Month < b.Month)
                {
                    return true;
                }

                return false;
            }

            public string Adjust(Date min)
            {
                int dm = Month - min.Month;
                int dy = (Year - min.Year) * 12;
                return (dm + dy).ToString();
            }
        }

        public override void Run(ResultsPackage rp)
        {
            List<Date> dates = new List<Date>();
            Date min = null;
            string outfile = base.Ffn(OutputFile, InputFile);

            using (StreamReader sr = new StreamReader(InputFile))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();

                    string[] elements = line.Split(",\t".ToCharArray());

                    string element = elements[Column];

                    string syear = element.Substring(element.Length - 2);
                    string sday = element.Substring(element.Length - 4, 2);
                    string smonth = element.Substring(0, element.Length - 4);

                    Debug.Assert(sday == "00");

                    int year = int.Parse(syear);
                    int month = int.Parse(smonth);
                    Date d = new Date(year, month, elements);
                    if (min == null || d.IsLower(min))
                    {
                        min = d;
                    }
                    dates.Add(d);
                }
            }

            using (StreamWriter sw = new StreamWriter(outfile))
            {
                foreach (Date date in dates)
                {
                    date.Elements[Column] = date.Adjust(min);

                    sw.WriteLine(string.Join(",", date.Elements));
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
