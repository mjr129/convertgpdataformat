﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("PPM Identification")]
    [Folder(FolderType.Nmr)]
    [Serializable]
    class PpmIdentification : Transformation
    {
        [DisplayName("TSP")]
        [Category("Behaviour")]
        [DefaultValue(0)]
        public int Tsp { get; set; }

        [DisplayName("SW")]
        [Category("Behaviour")]
        [DefaultValue(0d)]
        public double Sw { get; set; }

        [Category("Behaviour")]
        [DefaultValue(0)]
        public int VariableCount { get; set; }

        [Category("Input")]
        [DefaultValue(0)]
        public int Query { get; set; }

        public override void Run(ResultsPackage rp)
        {
            double result = (-Sw / (VariableCount)) * (Query - Tsp);

            rp.AddInfo("Result", result);
        }
    }
}
