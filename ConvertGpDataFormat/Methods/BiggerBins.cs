﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Make Bigger Bins")]
    [Description("Make bins bigger for binned data (2D or 3D).")]
    [Folder(FolderType.Nmr)]
    [Serializable]
    class BiggerBins : CsvInOutTransformation
    {
        [Description("Number of bins (X) where data in each row is arranged (1, 2 ... x) * y")]
        [Category("Inputs")]
        [DefaultValue(10)]
        public int SizeX { get; set; }

        [Description("Number of bins (Y) where data in each row is arranged (1, 2 ... x) * y. This is 1 for 2D data.")]
        [Category("Inputs")]
        [DefaultValue(1)]
        public int SizeY { get; set; }

        [Description("Number of times to reduce \"X\" by. 1 for no change, 2 for half the size, 3 for a third of the size, etc. The initial data must be wholly divisible.")]
        [Category("Outputs")]
        [DefaultValue(2)]
        public int DivideX { get; set; }

        [Description("Number of times to reduce \"Y\" by. 1 for no change, 2 for half the size, 3 for a third of the size, etc. The initial data must be wholly divisible.")]
        [Category("Outputs")]
        [DefaultValue(1)]
        public int DivideY { get; set; }

        int _totalValues;
        int _newSizeX;
        int _newSizeY;

        public override void Run(ResultsPackage rp)
        {
            _newSizeX = SizeX / DivideX;
            _newSizeY = SizeY / DivideY;
            _totalValues = SizeX * SizeY;

            if ((_newSizeX * DivideX) != SizeX)
            {
                throw new InvalidOperationException("XSize (" + SizeX + ") is not wholly divisible by DivideX (" + DivideX + ")");
            }

            if ((_newSizeY * DivideY) != SizeY)
            {
                throw new InvalidOperationException("YSize (" + SizeY + ") is not wholly divisible by DivideY (" + DivideY + ")");
            }

            base.Run(rp);
        }

        protected override void ProcessLine(ProcessLineArgs e)
        {
            if (e.Fields.Length != _totalValues)
            {
                throw new InvalidOperationException("Number of fields (" + e.Fields.Length + ") in line (#" + e.LineNumber + ") is not SizeX (" + SizeX + ") * SizeY (" + SizeY + ") ( = " + _totalValues + " )");
            }

            // Add to matrix
            double[] mat = new double[_newSizeX * _newSizeY];

            for (int y = 0; y < SizeY; y++)
            {
                for (int x = 0; x < SizeX; x++)
                {
                    double v = double.Parse(e.Fields[x + (SizeX * y)]);

                    int nX = x / DivideX;
                    int nY = y / DivideY;

                    mat[nX + (_newSizeX * nY)] += v;
                }
            }

            // Write new line
            WriteLine(e, mat);
        }
    }
}
