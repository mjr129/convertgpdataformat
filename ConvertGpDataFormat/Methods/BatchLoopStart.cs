﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Batch / Start Loop")]
    [Description("Starts a loop for a batch process, subsitituing a variable name for a value each iteration. Alternatively loops with a single iteration may be used to set arbitrary variable names for convenience. Variables may be accessed in certain fields e.g. \"c:\\dir\\file{LOOP}.txt\" where LOOP is the name of the variable.")]
    [Folder(FolderType.Batch)]
    [Serializable]
    class BatchLoopStart : Transformation
    {
        [Description("The name of this loop")]
        public string Name { get; set; }

        [Description("The values set within this loop.")]
        public string[] Values { get; set; }

        public override void Run(ResultsPackage rp)
        {
            throw new Exception("Loops cannot be run on their own. Use the Queue option under the batch processing menu to add loops to a batch process.");
        }
    }

    [DisplayName("Batch / End Loop")]
    [Description("Ends a batch loop")]
    [Folder(FolderType.Batch)]
    [Serializable]
    class BatchLoopEnd : Transformation
    {
        public override void Run(ResultsPackage rp)
        {
            throw new Exception("Loops cannot be run on their own. Use the Queue option under the batch processing menu to add loops to a batch process.");
        }
    }

    class BatchProcessor
    {
        Stack<BatchLoopEntry> _entries = new Stack<BatchLoopEntry>();

        class BatchLoopEntry
        {
            public string Name;
            public string[] Values;
            public int Current;

            public BatchLoopEntry(BatchLoopStart f)
            {
                Name = f.Name;
                Values = f.Values;
                Current = -1;
            }

            public BatchLoopValue Iterate()
            {
                Current += 1;

                if (Current < Values.Length)
                {
                    return new BatchLoopValue(Name, Values[Current]);
                }
                else
                {
                    return null;
                }
            }
        }

        public class BatchLoopValue
        {
            public string Name { get; set; }
            public string Value { get; set; }

            public BatchLoopValue(string name, string value)
            {
                Name = name;
                Value = value;
            }
        }

        public BatchLoopValue Add(BatchLoopStart bls)
        {
            var n = new BatchLoopEntry(bls);

            _entries.Push(n);

            return n.Iterate();
        }

        public BatchLoopValue Iterate()
        {
            var result = _entries.Peek().Iterate();

            if (result == null)
            {
                _entries.Pop();
            }

            return result;
        }
    }
}
