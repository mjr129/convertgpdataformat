﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Transpose CSV")]
    [Description("Flips rows and columns of CSV data.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class TransposeCsv : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File to read in")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH}{FILE}-transposed{EXT}")]
        public string OutputFile { get; set; }

        [Description("CSV separator")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string CsvSeparator { get; set; }

        [Description("Ignore empty cells (use if data has been justified by adding spaces, etc.).")]
        [Category("Inputs")]
        [DefaultValue(false)]
        public bool RemoveEmpties { get; set; }

        public TransposeCsv()
        {
            InputCsvSeparator = "{TAB}{SPACE},";
            CsvSeparator = ",";
            OutputFile = "{PATH}{FILE}-transposed{EXT}";
        }

        public override void Run(ResultsPackage rp)
        {
            List<string[]> data = new List<string[]>();
            char[] csvsep = base.Escape(InputCsvSeparator).ToCharArray();
            string csvsepo = base.Escape(CsvSeparator);
            string outfile = base.Ffn(OutputFile, InputFile);
            StringSplitOptions sso = RemoveEmpties ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            using (StreamReader sr = new StreamReader(InputFile))
            {
                while (!sr.EndOfStream)
                {
                    string l = sr.ReadLine();
                    string[] e = l.Split(csvsep, sso);

                    data.Add(e);
                }
            }

            for (int row = 1; row < data.Count; row++)
            {
                if (data[row].Length != data[row - 1].Length)
                {
                    throw new Exception("ERROR: Data will not transpose correctly. Not all rows have an equal length. E.g. row " + (row - 1) + " has a length of " + data[row - 1].Length + " and row " + (row) + " has a length of " + data[row].Length + ". Check the \"input CSV separator\" and \"remove empties\" options.");
                }
            }

            using (StreamWriter sw = new StreamWriter(outfile))
            {
                int column = 0;
                bool eod = false;

                while (true)
                {
                    for (int row = 0; row < data.Count; row++)
                    {
                        if (data[row].Length <= column)
                        {
                            eod = true;
                            break;
                        }

                        sw.Write(data[row][column]);

                        if (row != data.Count - 1)
                        {
                            sw.Write(csvsepo);
                        }
                    }

                    if (eod)
                    {
                        break;
                    }

                    sw.WriteLine();
                    column++;
                }
            }

            rp.AddInfo("Rows", data.Count);
            rp.AddInfo("Columns", data[0].Length);
            rp.AddOutput(outfile);
        }
    }
}
