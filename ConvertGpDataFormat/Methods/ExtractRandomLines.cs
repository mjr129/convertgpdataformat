﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Obsolete")]
    [Description("Randomly splits the lines from the input file into two output files.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class ExtractRandomLines : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Input file (MJR-GP format). Must contain only 2 classes.")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        public string TrainingOutputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        public string ValidationOutputFile { get; set; }

        [Category("Outputs")]
        [Description("What fraction of the data goes into the validation output file.")]
        [DefaultValue(0.25)]
        public double ValidationFraction { get; set; }

        public ExtractRandomLines()
        {
            InputFile = "";
            TrainingOutputFile = "";
            ValidationOutputFile = "";
            ValidationFraction = 0.25;
        }

        public override void Run(ResultsPackage rp)
        {
            List<string> lines1 = new List<string>(File.ReadAllLines(InputFile));
            List<string> lines2 = new List<string>();

            for (int n = 0; n < lines1.Count; n++)
            {
                if (lines1[n].Length == 0)
                {
                    lines1.RemoveAt(n);
                    n--;
                }
            }

            Random rnd = new Random();

            int count = (int)(ValidationFraction * lines1.Count);

            for (int n = 0; n < count; n++)
            {
                int index = rnd.Next(lines1.Count);

                lines2.Add(lines1[index]);
                lines1.RemoveAt(index);
            }

            File.WriteAllLines(TrainingOutputFile, lines1);
            File.WriteAllLines(ValidationOutputFile, lines2);

            rp.Add("Training", TrainingOutputFile, ResultsType.FileName);
            rp.Add("Validation", ValidationOutputFile, ResultsType.FileName);
        }
    }
}
