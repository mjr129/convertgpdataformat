﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Concatenate lines")]
    [Description("Concatenates the lines from two input files\r\n<output line> = <left line>,<right line>\r\nThe output file will contain the number of lines found in the shortest file.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class ConcatenateLines : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the left side of the lines are taken from")]
        [DefaultValue("")]
        public string LeftInputFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the right side of the lines are taken from")]
        [DefaultValue("")]
        public string RightInputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH1}{FILE1}+{FILE2}{EXT1}")]
        public string OutputFile { get; set; }

        [Description("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string CsvSeparator { get; set; }

        public ConcatenateLines()
        {
            OutputFile = "{PATH1}{FILE1}+{FILE2}{EXT1}";
            CsvSeparator = ",";
        }

        public override void Run(ResultsPackage rp)
        {
            string outFileName = Ffn(OutputFile, LeftInputFile, RightInputFile);
            string csvSep = Escape(CsvSeparator);

            using (StreamReader left = new StreamReader(LeftInputFile))
            {
                using (StreamReader right = new StreamReader(RightInputFile))
                {
                    using (StreamWriter output = new StreamWriter(outFileName))
                    {
                        while (!left.EndOfStream && !right.EndOfStream)
                        {
                            output.Write(left.ReadLine());
                            output.Write(csvSep);
                            output.WriteLine(right.ReadLine());
                        }
                    }
                }
            }

            rp.AddOutput(outFileName);
        }
    }
}
