﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Arrange files by class")]
    [Description("Move files into folders based on class")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class ArrangeFilesByClass : Transformation
    {
        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Directory containing the input files")]
        [DefaultValue("")]
        public string InputDirectory { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Input file (CSV format)")]
        [DefaultValue("")]
        public string ClassFile { get; set; }

        [Category("Inputs")]
        [Description("Input file delimiters")]
        [DefaultValue("{SPACE}{TAB},")]
        public string ClassFileDelimiters { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file names. Accepts standard macros (see \"More\" menu for help). 1 = based on InputDirectory, 2 = based on ClassFile, 3 = Original file name. Additional macros are {CLASS} and {ID} taken from ClassColumnIndex and IdColumnIndex of ClassFile respectively.")]
        [DefaultValue("{DIR1}byclass\\{CLASS}\\{FILE3}{EXT3}")]
        public string OutputPattern { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Input file names. Accepts standard macros (see \"More\" menu for help). 1 = based on InputDirectory, 2 = based on ClassFile. Additional macros are {CLASS} and {ID} taken from ClassColumnIndex and IdColumnIndex of ClassFile respectively.")]
        [DefaultValue("{DIR1}{ID}.cdf")]
        public string InputPattern { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Where to save report. Accepts standard macros (see \"More\" menu for help). 1 = based on InputDirectory, 2 = based on ClassFile.")]
        [DefaultValue("{DIR1}MovedFiles.txt")]
        public string ReportFile { get; set; }

        [Category("Inputs")]
        [Description("Index of column to use for class determination.")]
        [DefaultValue(4)]
        public int ClassColumnIndex { get; set; }

        [Category("Inputs")]
        [Description("Index of column to use for file determination.")]
        [DefaultValue(0)]
        public int IdColumnIndex { get; set; }

        [Category("Inputs")]
        [Description("Whether the class file contains headers.")]
        [DefaultValue(true)]
        public bool ClassHeaders { get; set; }

        [Category("Outputs")]
        [Description("When test mode is active no files will be moved.")]
        [DefaultValue(true)]
        public bool Test { get; set; }

        public ArrangeFilesByClass()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            char[] csvSep = base.Escape(ClassFileDelimiters).ToCharArray();
            StringBuilder report = new StringBuilder();
            string firstOutFile = null;
            string outFile = "";
            string reportFile = base.Ffn(ReportFile, InputDirectory, ClassFile);

            using (StreamWriter sw = new StreamWriter(reportFile))
            {
                using (StreamReader sr = new StreamReader(ClassFile))
                {
                    if (ClassHeaders)
                    {
                        sr.ReadLine();
                    }

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            string[] e = line.Split(csvSep);

                            string id = e[IdColumnIndex];
                            string c = e[ClassColumnIndex];

                            string inFile = base.Ffn(InputPattern, InputDirectory, ClassFile);
                            outFile = base.Ffn(OutputPattern, InputDirectory, ClassFile, inFile);

                            inFile = inFile.Replace("{ID}", id);
                            outFile = outFile.Replace("{ID}", id);
                            inFile = inFile.Replace("{CLASS}", c);
                            outFile = outFile.Replace("{CLASS}", c);

                            sw.WriteLine(inFile + "\t" + outFile);

                            if (!Test)
                            {
                                Directory.CreateDirectory(Path.GetDirectoryName(outFile));
                                File.Move(inFile, outFile);
                            }

                            if (firstOutFile == null)
                            {
                                firstOutFile = outFile;
                            }
                        }
                    }
                }
            }

            rp.Add("Test mode", Test.ToString(), ResultsType.Information);
            rp.Add("Report", reportFile, ResultsType.FileName);
            rp.Add("First file", firstOutFile ?? "", ResultsType.FileName);
            rp.Add("Last file", outFile, ResultsType.FileName);
        }
    }
}
