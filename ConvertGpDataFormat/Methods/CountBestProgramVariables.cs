﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Count variables in best programs")]
    [Description("Count variables found in the best program trees from a GP run.")]
    [Folder(FolderType.Gp)]
    [Serializable]
    class CountBestProgramVariables : Transformation
    {
        public enum SortOrder
        {
            Variables,
            Usages
        }

        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Directory will be recursivley searched for best_program.csv files.")]
        [DefaultValue("")]
        public string InputDirectory { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file, macros are accepted.")]
        [DefaultValue("{PATH}VariablePickFrequencies.csv")]
        public string OutputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Sort mode")]
        [DefaultValue(SortOrder.Usages)]
        public SortOrder SortMode { get; set; }

        [Category("Outputs")]
        [Description("Whether to include variables never used (based on environment.csv files)")]
        [DefaultValue(false)]
        public bool IncludeZeroes { get; set; }

        [Category("Outputs")]
        [Description("Whether to detail the number of programs considered before the headers in the output file")]
        [DefaultValue(true)]
        public bool IncludeProgramCount { get; set; }

        [Category("Inputs")]
        [Description("Only include files with two stage inputs present.")]
        [DefaultValue(false)]
        public bool OnlyIfTwoStagePresent { get; set; }

        public CountBestProgramVariables()
        {
            InputDirectory = "";
            OutputFile = "{PATH}VariablePickFrequencies.csv";
            SortMode = SortOrder.Usages;
            IncludeZeroes = false;
            IncludeProgramCount = true;
        }

        public int GetFileSize(string fileName)
        {
            return (int)new FileInfo(fileName).Length;
        }

        public override void Run(ResultsPackage rp)
        {
            Dictionary<int, int> counts = new Dictionary<int, int>();

            if (IncludeZeroes)
            {
                int variable_count = CountVariables();

                for (int n = 0; n < variable_count; n++)
                {
                    counts.Add(n, 0);
                }
            }

            string[] files = Directory.GetFiles(InputDirectory, "best_program.csv", SearchOption.AllDirectories);
            int total = 0;

            foreach (string file in files)
            {
                if (OnlyIfTwoStagePresent)
                {
                    string twoStageFile = Path.Combine(
                                Path.GetDirectoryName(file),
                                "stage_2_inputs.csv");
                    if (!File.Exists(twoStageFile)
                        || GetFileSize(twoStageFile) == 7)
                    {
                        continue;
                    }
                }

                total++;

                List<int> variables = GetVariablesUsed(file);

                foreach (int variable in variables)
                {
                    if (counts.ContainsKey(variable))
                    {
                        counts[variable] = counts[variable] + 1;
                    }
                    else
                    {
                        counts.Add(variable, 1);
                    }
                }
            }

            List<KeyValuePair<int, int>> counts2 = new List<KeyValuePair<int, int>>(counts);

            switch (SortMode)
            {
                case SortOrder.Variables:
                    counts2.Sort((x, y) => x.Key.CompareTo(y.Key));
                    break;

                case SortOrder.Usages:
                    counts2.Sort((x, y) => -x.Value.CompareTo(y.Value));
                    break;
            }

            string outputFile = FfnDir(OutputFile, InputDirectory);

            using (StreamWriter sw = new StreamWriter(outputFile))
            {
                if (IncludeProgramCount)
                {
                    if (OnlyIfTwoStagePresent)
                    {
                        sw.WriteLine("RESULTS OF " + total + " ATTEMPTS (" + (files.Length - total) + " out of " + files.Length + " attempts ignored as they did not reach the second stage)");
                    }
                    else
                    {
                        sw.WriteLine("RESULTS OF " + total + " ATTEMPTS");
                    }
                }
                sw.WriteLine("variable, count, fraction");

                foreach (var kvp in counts2)
                {
                    sw.WriteLine(kvp.Key + ", " + kvp.Value + ", " + ((double)kvp.Value / files.Length).ToString("G02"));
                }
            }

            rp.AddOutput(outputFile);
        }

        private int CountVariables()
        {
            string[] files = Directory.GetFiles(InputDirectory, "environment.csv", SearchOption.AllDirectories);
            int result = 0;

            foreach (string file in files)
            {
                using (StreamReader sr = new StreamReader(file))
                {
                    while (!sr.EndOfStream)
                    {
                        string[] elements = sr.ReadLine().Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        if (elements.Length > 1 && elements[0] == "Variables per observation")
                        {
                            result = Math.Max(result, int.Parse(elements[1]));
                        }
                    }
                }
            }

            return result;
        }

        private static List<int> GetVariablesUsed(string fileName)
        {
            List<int> result = new List<int>();

            using (StreamReader sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();

                    line = line.TrimStart(" \t,".ToCharArray());

                    if (line.StartsWith("V:"))
                    {
                        line = line.Substring(2);

                        int value = int.Parse(line);

                        if (!result.Contains(value))
                        {
                            result.Add(value);
                        }
                    }
                }
            }
            return result;
        }
    }
}
