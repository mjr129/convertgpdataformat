﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Extract fields")]
    [Description("Creates a table of value(s) from CSV file(s) and puts them into one file. (One row per field, one column per file).")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class ExtractField : Transformation
    {
        [EditorAttribute(typeof(OpenMultipleFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Filenames")]
        [Category("Inputs")]
        [Description("Files to read")]
        [TypeConverter(typeof(StringListConvertor))]
        [DefaultValue("")]
        public string[] InputFiles { get; set; }

        [DisplayName("Column index")]
        [Category("Inputs")]
        [Description("Comma delimited list of column index(es) containing the value(s) to extract. Index is zero-based. If more than one column is specified both values will be extracted separately and given unique names (e.g. \"RowName#1\" for the first value and \"RowName#2\" for the second - normally just \"RowName\" is used.)")]
        [DefaultValue("2")]
        public string ValueColumns { get; set; }

        [DisplayName("Row names")]
        [Category("Inputs")]
        [Description("The names (text in first column) of rows from which values will be extracted, other rows will be ignored). If blank then all rows will be extracted.")]
        [DefaultValue(new string[0])]
        public string[] RowNames { get; set; }

        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Filename")]
        [Category("Outputs")]
        [Description("File to write output to")]
        [DefaultValue("{PATH}{FILE}-fields{EXT}")]
        public string OutputFile { get; set; }

        [Description("CSV separator for output file")]
        [DisplayName("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string OutputCsvSeparator { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [DisplayName("CSV separator(s)")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("Ignore empty cells (use if data has been justified by adding spaces, etc.).")]
        [DisplayName("Ignore empty columns")]
        [Category("Inputs")]
        [DefaultValue(false)]
        public bool RemoveEmpties { get; set; }

        [Description("Transpose output.")]
        [DisplayName("Display files along side rather than top.")]
        [Category("Outputs")]
        [DefaultValue(true)]
        public bool Transpose { get; set; }

        [Description("Append output.")]
        [DisplayName("Append to output rather than overwrite.")]
        [Category("Outputs")]
        [DefaultValue(false)]
        public bool Append { get; set; }

        [Description("Deletes source files afterwards.")]
        [DisplayName("DELETE SOURCE FILES")]
        [Category("More options")]
        [DefaultValue(false)]
        public bool DeleteSourceFiles { get; set; }

        [Description("Ignores errors.")]
        [DisplayName("Ignore errors")]
        [Category("More options")]
        [DefaultValue(false)]
        public bool IgnoreErrors { get; set; }

        [Description("Fields with this name will be used as a prefix to subsequent fields in the file (use if the file has sections).")]
        [DisplayName("Prefix field")]
        [Category("Inputs")]
        [DefaultValue("TESTNAME")]
        public string PrefixField { get; set; }

        private int[] GetValueColumns()
        {
            string[] colsS = ValueColumns.Split(",; ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            if (colsS.Length == 0)
            {
                throw new FormatException("At least one input column must be specified.");
            }

            int[] colsI = new int[colsS.Length];

            for (int n = 0; n < colsS.Length; n++)
            {
                if (!int.TryParse(colsS[n], out colsI[n]))
                {
                    throw new FormatException("Column \"" + colsS[n] + "\" is not a number.");
                }
            }

            return colsI;
        }

        public override void Run(ResultsPackage rp)
        {
            char[] csvSeparatorsIn = base.Escape(InputCsvSeparator).ToCharArray();
            string csvSeparatorOut = base.Escape(OutputCsvSeparator);
            string outputFile = base.Ffn(OutputFile, InputFiles);
            StringSplitOptions stringSplitOptions = RemoveEmpties ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;
            int[] valueColumns = GetValueColumns();

            List<Tuple<string, string[]>> results = new List<Tuple<string, string[]>>();

            for (int i = 0; i < InputFiles.Length; i++)
            {
                string file = InputFiles[i];

                AddToList(file, results, "FilePath", file, i, InputFiles.Length);
                AddToList(file, results, "FileName", Path.GetFileNameWithoutExtension(file), i, InputFiles.Length);
                string prefix = "";

                using (StreamReader sr = new StreamReader(file))
                {
                    while (!sr.EndOfStream)
                    {
                        rp.ReportProgress(sr, i, InputFiles.Length);

                        string line = sr.ReadLine();
                        string[] e = line.Split(csvSeparatorsIn, stringSplitOptions);
                        string field = e[0];

                        if (field == PrefixField)
                        {
                            prefix = e[1];
                        }

                        if (!string.IsNullOrWhiteSpace(prefix))
                        {
                            field = prefix + " - " + field;
                        }

                        if ((RowNames == null || RowNames.Length == 0 || RowNames.Contains(field)))
                        {
                            foreach (int valueColumn in valueColumns)
                            {
                                if (e.Length > valueColumn)
                                {
                                    string fieldName;
                                    string value = e[valueColumn];

                                    if (valueColumns.Length == 1)
                                    {
                                        fieldName = field;
                                    }
                                    else
                                    {
                                        fieldName = field + " (" + valueColumn + ")";
                                    }

                                    AddToList(file, results, fieldName, value, i, InputFiles.Length);
                                }
                            }
                        }
                    }
                }

                foreach (var x in results)
                {
                    if (x.Item2[i] == null)
                    {
                        if (!IgnoreErrors)
                        {
                            throw new Exception("File [" + file + "] #" + i + " does not have a value for [" + x.Item1 + "] whilst previous files (e.g. [" + InputFiles[i - 1] + "]) do.");
                        }
                    }
                }
            }

            using (StreamWriter sw = new StreamWriter(outputFile, Append))
            {
                if (Transpose)
                {
                    // Headings (fields)
                    bool needsComma = false;

                    foreach (var x in results)
                    {
                        if (needsComma)
                        {
                            sw.Write(",");
                        }
                        else
                        {
                            needsComma = true;
                        }

                        sw.Write(InCsv(x.Item1));
                    }

                    sw.WriteLine();

                    // Values for each files in turn
                    for (int i = 0; i < InputFiles.Length; i++)
                    {
                        needsComma = false;

                        foreach (var x in results)
                        {
                            if (needsComma)
                            {
                                sw.Write(",");
                            }
                            else
                            {
                                needsComma = true;
                            }

                            sw.Write(InCsv(x.Item2[i]));
                        }

                        sw.WriteLine();
                    }
                }
                else
                {
                    // Values for each field in turn
                    foreach (var x in results)
                    {
                        sw.Write(InCsv(x.Item1));

                        foreach (var y in x.Item2)
                        {
                            sw.Write(",");
                            sw.Write(InCsv(y));
                        }

                        sw.WriteLine();
                    }
                }
            }

            if (DeleteSourceFiles)
            {
                foreach (string file in InputFiles)
                {
                    File.Delete(file);
                }

                rp.AddInfo("Files deleted", InputFiles.Length);
            }

            rp.AddOutput(outputFile);
            rp.AddInfo("Files", InputFiles.Length);
            rp.AddInfo("Fields", results.Count);
        }

        private string InCsv(string x)
        {
            if (x == null)
            {
                return "";
            }

            if (x.Length == 0 || !char.IsNumber(x[0]))
            {
                return "\"" + x + "\"";
            }

            return x;
        }

        private void AddToList(string file, List<Tuple<string, string[]>> results, string field, string value, int i, int mx)
        {
            Tuple<string, string[]> x = null;

            foreach (var r in results)
            {
                if (r.Item1 == field)
                {
                    x = r;
                    break;
                }
            }

            if (x == null)
            {
                if (i != 0)
                {
                    if (!IgnoreErrors)
                    {
                        throw new Exception("File [" + file + "] #" + i + " contains a value for field [" + field + "] not seen in files previous.");
                    }
                }

                x = new Tuple<string, string[]>(field, new string[mx]);

                results.Add(x);
            }

            if (x.Item2[i] != null)
            {
                if (!IgnoreErrors)
                {
                    throw new Exception("File [" + file + "] #" + i + " contains multiple values for [" + field + "].");
                }
            }

            x.Item2[i] = value;
        }
    }
}
