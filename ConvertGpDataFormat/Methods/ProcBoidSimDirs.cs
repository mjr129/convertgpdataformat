﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Single value statistics for Boids")]
    [Description("Takes inputs from a specific line and column of a specific file and adds them to a CSV alongside the number taken from the directory name of the form \"x-n--xx\" where n is a double.")]
    [Folder(FolderType.Boids)]
    [Serializable]
    class ProcBoidSimDirs : Transformation
    {
        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Directory will be recursivley searched for best_program.csv files.")]
        [DefaultValue("")]
        public string InputDirectory { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file. Macros are accepted.")]
        [DefaultValue("{PATH}STATS.TXT")]
        public string OutputFile { get; set; }

        [Category("Inputs")]
        [Description("Name of input files")]
        [DefaultValue("OUT-VariousMi.csv")]
        public string InputFile { get; set; }

        public ProcBoidSimDirs()
        {
            base.ApplyDefaultsFromAttributes();
        }

        class ValueCollection
        {
            Dictionary<string, int> allkeys = new Dictionary<string, int>();
            Dictionary<string, Dictionary<string, double>> datas = new Dictionary<string, Dictionary<string, double>>();
            Dictionary<string, double> cdata;

            public ValueCollection()
            {
                allkeys["File"] = 1;
                allkeys["Count"] = 1;
                allkeys["One"] = 1;
            }

            internal void NewEntry(string key)
            {
                if (datas.ContainsKey(key))
                {
                    cdata = datas[key];
                    cdata["Count"] = cdata["Count"] + 1;
                }
                else
                {
                    cdata = new Dictionary<string, double>();
                    cdata.Add("File", double.Parse(key));
                    cdata.Add("Count", 1);
                    cdata.Add("One", 1);
                    datas.Add(key, cdata);
                }
            }

            internal void AddValue(string p1, string p2)
            {
                if (cdata.ContainsKey(p1))
                {
                    cdata[p1] = cdata[p1] + double.Parse(p2);
                }
                else
                {
                    cdata.Add(p1, double.Parse(p2));
                }

                allkeys[p1] = 1;
            }

            public string[] GetKeys()
            {
                string[] keys = allkeys.Keys.ToArray<string>();

                Array.Sort<string>(keys, SorterKeys);

                return keys;
            }

            internal List<Dictionary<string, double>> GetValues()
            {
                List<Dictionary<string, double>> results = new List<Dictionary<string, double>>();

                foreach (var kvp in datas)
                {
                    foreach (var skvp in kvp.Value.ToArray())
                    {
                        if (skvp.Key != "File" && skvp.Key != "Count")
                        {
                            kvp.Value[skvp.Key] = kvp.Value[skvp.Key] / kvp.Value["Count"];
                        }
                    }

                    results.Add(kvp.Value);
                }

                results.Sort(Sorter);

                return results;
            }

            int Sorter(Dictionary<string, double> a, Dictionary<string, double> b)
            {
                double av = a["File"];
                double bv = b["File"];
                return av.CompareTo(bv);
            }

            int SorterKeys(string a, string b)
            {
                if (a == "File")
                {
                    if (b == "File")
                    {
                        return 0;
                    }

                    return -1;
                }
                else if (b == "File")
                {
                    return 1;
                }
                else
                {
                    return a.CompareTo(b);
                }
            }
        }

        public override void Run(ResultsPackage rp)
        {
            string[] files = Directory.GetFiles(InputDirectory, InputFile, SearchOption.AllDirectories);
            string outfile = Ffn(OutputFile, Path.Combine(InputDirectory, "dir.txt"));
            ValueCollection vc = new ValueCollection();

            foreach (string file in files)
            {
                string dirName = Path.GetFileName(Path.GetDirectoryName(file));
                string[] dirNameE = dirName.Split("-".ToCharArray());
                string dirNameK = double.Parse(dirNameE[1]).ToString("F02");
                vc.NewEntry(dirNameK);

                using (StreamReader sr = new StreamReader(file))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] elements = line.Split(",".ToCharArray());

                        vc.AddValue(elements[0] + "_E0", elements[1]);
                        vc.AddValue(elements[0] + "_E1", elements[2]);
                        vc.AddValue(elements[0] + "_MI", elements[3]);
                    }
                }
            }

            var vals = vc.GetValues();

            string[] keys = vc.GetKeys();

            using (StreamWriter sw = new StreamWriter(outfile))
            {
                for (int n = 0; n < keys.Length; n++)
                {
                    if (n != 0)
                    {
                        sw.Write(",");
                    }
                    sw.Write(keys[n]);
                }

                sw.WriteLine();

                foreach (var v in vals)
                {
                    for (int n = 0; n < keys.Length; n++)
                    {
                        if (n != 0)
                        {
                            sw.Write(",");
                        }
                        sw.Write(v[keys[n]]);
                    }

                    sw.WriteLine();
                }
            }

            rp.AddInfo("FileCount", files.Length.ToString());
            rp.AddInfo("ValueCount", keys.Length.ToString());
            rp.AddOutput(outfile);
        }
    }
}
