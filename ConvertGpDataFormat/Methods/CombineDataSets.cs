﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Combine data sets")]
    [Description("Combine datasets. This is identical to ConcatenateData but throws an error if keys don't match.")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class CombineDataSets : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the left side of the lines are taken from")]
        [DefaultValue("")]
        public string LeftInputFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the right side of the lines are taken from")]
        [DefaultValue("")]
        public string RightInputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH1}{FILE1}+{FILE2}{EXT1}")]
        public string OutputFile { get; set; }

        [Description("CSV separator")]
        [Category("Outputs")]
        [DefaultValue(",")]
        public string CsvSeparator { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        public CombineDataSets()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            string outFileName = Ffn(OutputFile, LeftInputFile, RightInputFile);
            string inCsvSep = Escape(InputCsvSeparator);
            string csvSep = Escape(CsvSeparator);
            int lines = 0;

            using (StreamReader left = new StreamReader(LeftInputFile))
            {
                using (StreamReader right = new StreamReader(RightInputFile))
                {
                    using (StreamWriter output = new StreamWriter(outFileName))
                    {
                        while (!left.EndOfStream && !right.EndOfStream)
                        {
                            lines++;

                            string leftLine = left.ReadLine();
                            string rightLine = right.ReadLine();

                            if (leftLine.Length == 0 || rightLine.Length == 0)
                            {
                                if (leftLine.Length != 0 || rightLine.Length != 0)
                                {
                                    throw new KeyNotFoundException("Empty line found in one file only (at line " + lines + ").");
                                }

                                continue;
                            }

                            string[] leftElements = leftLine.Split(inCsvSep.ToCharArray(), 2);
                            string[] rightElements = rightLine.Split(inCsvSep.ToCharArray(), 2);

                            if (leftElements[0] != rightElements[0])
                            {
                                throw new KeyNotFoundException("Key column mismatch in file at line " + lines + ": \"" + leftElements[0] + "\" vs \"" + rightElements[0] + "\".");
                            }

                            output.Write(leftElements[0]);
                            output.Write(csvSep);
                            output.Write(leftElements[1]);
                            output.Write(csvSep);
                            output.WriteLine(rightElements[1]);
                        }

                        if (!left.EndOfStream || !right.EndOfStream)
                        {
                            throw new KeyNotFoundException("Input files are of different length.");
                        }
                    }
                }
            }

            rp.AddInfo("Lines", lines);
            rp.AddOutput(outFileName);
        }
    }
}
