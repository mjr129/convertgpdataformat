﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Obsolete")]
    [Description("Randomly splits the lines from the input file into two output files. Lines are split in a manner such that the percentage of data of a particular class in the output files matches the percentage found in the original file.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class ExtractRandom : Transformation
    {
        public enum HeadersMode
        {
            None,
            Input,
            InputAndOutput,
        };

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Input file (CSV, TSV, SSV, format, fields merged). Must contain only 2 classes.")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        public string TrainingOutputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DefaultValue("")]
        public string ValidationOutputFile { get; set; }

        [Category("Behaviour")]
        [Description("What fraction of the data goes into the validation file.")]
        [DefaultValue(0.25)]
        public double ValidationFraction { get; set; }

        [Category("Behaviour")]
        [Description("Which column contains the class, or -1 to preserve no class ratio")]
        [DefaultValue(-1)]
        public int ClassColumn { get; set; }

        [Category("Behaviour")]
        [Description("Whether to input and output headers")]
        [DefaultValue(HeadersMode.None)]
        public HeadersMode Headers { get; set; }

        [Category("Behaviour")]
        [Description("Variable numbers to always force into the validation set")]
        public List<int> ForceValidation { get; set; }

        public ExtractRandom()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            List<KeyValuePair<string, string>> lines = new List<KeyValuePair<string, string>>();
            Dictionary<string, int> classes = new Dictionary<string, int>();
            string headers = "";

            using (StreamReader sr = new StreamReader(InputFile))
            {
                if (Headers != HeadersMode.None && !sr.EndOfStream)
                {
                    headers = sr.ReadLine();
                }

                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();

                    if (line.Length != 0)
                    {
                        string @class = GetClass(line);

                        if (!classes.ContainsKey(@class))
                        {
                            classes.Add(@class, 1);
                        }
                        else
                        {
                            classes[@class] = classes[@class] + 1;
                        }

                        lines.Add(new KeyValuePair<string, string>(@class, line));
                    }
                }
            }

            List<KeyValuePair<string, string>> vlines = new List<KeyValuePair<string, string>>();

            int validLength = (int)(lines.Count * ValidationFraction);
            int trainLength = lines.Count - validLength;

            Debug.Assert((validLength + trainLength) == lines.Count);

            // FORCE VALIDATION
            ForceValidation.Sort();

            for (int n = ForceValidation.Count - 1; n >= 0; n--)
            {
                int i = ForceValidation[n];

                vlines.Add(lines[i]);
                lines.RemoveAt(i);
            }

            validLength -= ForceValidation.Count;

            Random rnd = new Random();

            foreach (var kvp in classes)
            {
                int classQuant = kvp.Value;
                double classRatio = (double)classQuant / (double)lines.Count;
                int validQuant = (int)(classRatio * validLength);

                for (int n = 0; n < validQuant; n++)
                {
                    int i = rnd.Next(lines.Count);
                    while (lines[i].Key != kvp.Key)
                    {
                        i = rnd.Next(lines.Count);
                    }

                    vlines.Add(lines[i]);
                    lines.RemoveAt(i);
                }
            }

            WriteFile(headers, lines, TrainingOutputFile, rp, "T");
            WriteFile(headers, vlines, ValidationOutputFile, rp, "V");
        }

        private void WriteFile(string headers, List<KeyValuePair<string, string>> lines, string fn, ResultsPackage rp, string rpprefix)
        {
            Dictionary<string, int> cla = new Dictionary<string, int>();

            using (StreamWriter sw = new StreamWriter(fn))
            {
                if (Headers == HeadersMode.InputAndOutput)
                {
                    sw.WriteLine(headers);
                }

                foreach (var kvp in lines)
                {
                    sw.WriteLine(kvp.Value);

                    if (!cla.ContainsKey(kvp.Key))
                    {
                        cla.Add(kvp.Key, 1);
                    }
                    else
                    {
                        cla[kvp.Key] = cla[kvp.Key] + 1;
                    }
                }
            }

            int total = 0;

            foreach (var kvp in cla)
            {
                rp.AddInfo(rpprefix + " Class (" + kvp.Key + ")", kvp.Value.ToString());
                total += kvp.Value;
            }

            rp.AddInfo(rpprefix + " Total", total.ToString());
            rp.Add(rpprefix + " Output", fn, ResultsType.FileName);
        }

        private string GetClass(string line)
        {
            if (ClassColumn == -1)
            {
                return string.Empty;
            }

            string[] e = line.Split("\t ,".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            return e[ClassColumn];
        }
    }
}
