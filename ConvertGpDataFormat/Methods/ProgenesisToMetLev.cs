﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Progenesis to MetaboliteLevels")]
    [Description("Converts Progenesis data to MetaboliteLevels data.")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class ProgenesisToMetLev : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Progenesis data in")]
        [Category("Inputs")]
        [Description("File to read (progenesis data)")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Value matrix out")]
        [Category("Outputs")]
        [Description("File to create (value matrix)")]
        [DefaultValue("{PATH}Data.csv")]
        public string DataFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Observation information out")]
        [Category("Outputs")]
        [Description("File to create (information on observations)")]
        [DefaultValue("{PATH}Info.csv")]
        public string InfoFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Variable information out")]
        [Category("Outputs")]
        [Description("File to create (information on variables)")]
        [DefaultValue("{PATH}Info.csv")]
        public string VarInfoFile { get; set; }

        class DataFrame
        {
            public string[] ColHeaders;
            public string[] RowHeaders;
            public List<string[]> rows = new List<string[]>();

            public void AddRow(string[] e, int start, int end)
            {
                int l = end - start;

                if (HasRows && NumColumns != l)
                {
                    throw new FormatException("Row " + rows.Count + " is of different length in new dataframe.");
                }

                string[] n = new string[l];

                Array.Copy(e, start, n, 0, n.Length);

                rows.Add(n);
            }

            internal bool HasRows
            {
                get
                {
                    return rows.Count != 0;
                }
            }

            internal int NumColumns
            {
                get
                {
                    if (!HasRows)
                    {
                        throw new Exception("Cannot calculate num columns - no data.");
                    }

                    return rows[0].Length;
                }
            }

            internal int NumRows
            {
                get
                {
                    if (!HasRows)
                    {
                        throw new Exception("Cannot calculate num rows - no data.");
                    }

                    return rows[0].Length;
                }
            }

            internal void SetColHeaders(string[] s, int skip, Converter<string, string> c)
            {
                if (ColHeaders != null)
                {
                    throw new Exception("Cannot specify ColHeaders twice.");
                }

                ColHeaders = new string[NumColumns];

                for (int n = 0; n < ColHeaders.Length; n++)
                {
                    ColHeaders[n] = c(s[n + skip]);
                }
            }

            internal void SetRowHeaders(string[] s, int skip, Converter<string, string> c)
            {
                if (RowHeaders != null)
                {
                    throw new Exception("Cannot specify RowHeaders twice.");
                }

                RowHeaders = new string[NumRows];

                for (int n = 0; n < RowHeaders.Length; n++)
                {
                    RowHeaders[n] = c(s[n + skip]);
                }
            }

            internal static DataFrame Load(string file)
            {
                DataFrame df = new DataFrame();

                using (StreamReader sr = new StreamReader(file))
                {
                    while (!sr.EndOfStream)
                    {
                        string l = sr.ReadLine();
                        string[] e = l.Split(',');
                        df.AddRow(e, 0, e.Length);
                    }
                }

                return df;
            }

            internal void Save(string file, bool transpose)
            {
                using (StreamWriter sw = new StreamWriter(file))
                {
                    if (transpose)
                    {
                        foreach (string s in RowHeaders)
                        {
                            sw.Write(",");
                            sw.Write(s);
                        }

                        for (int x = 0; x < NumColumns; x++)
                        {
                            sw.Write(ColHeaders[x]);

                            for (int y = 0; y < NumRows; y++)
                            {
                                sw.Write(",");
                                sw.Write(rows[y][x]);
                            }
                        }
                    }
                    else
                    {
                        foreach (string s in ColHeaders)
                        {
                            sw.Write(",");
                            sw.Write(s);
                        }

                        for (int y = 0; y < NumRows; y++)
                        {
                            sw.Write(RowHeaders[y]);

                            for (int x = 0; x < NumColumns; x++)
                            {
                                sw.Write(",");
                                sw.Write(rows[y][x]);
                            }
                        }
                    }
                }
            }
        }

        public override void Run(ResultsPackage rp)
        {
            string fnIn = InputFile;
            string fnDataOut = base.Ffn(DataFile, fnIn);
            string fnInfoOut = base.Ffn(InfoFile, fnIn);
            string fnVarInfoOut = base.Ffn(VarInfoFile, fnIn);

            DataFrame dfInfo = new DataFrame();
            List<string> dfInfoH = new List<string>();
            DataFrame dfVarInfo = new DataFrame();
            string[] dfDataH;
            DataFrame dfData = new DataFrame();
            int f;

            DataFrame df = DataFrame.Load(fnIn);

            using (StreamReader sr = new StreamReader(fnIn))
            {
                rp.ReportProgress(sr);

                string[] e = Split(sr.ReadLine());

                if (e[0] != "")
                {
                    throw new FormatException("I don't understand this file. The first value (top left cell) should be empty.");
                }

                // The first non-empty value signifies the start of the data columns
                f = Propogate(e);

                // Add new rows to INFO until the first element is not empty
                // These rows read something like this:
                // , , , , , , Normalised abundance, , , , , , , Raw abundance
                // [empty]     [ file info block ]
                while (e[0] == "")
                {
                    dfInfo.AddRow(e, f, e.Length - 1);
                    dfInfoH.Add(e[f - 1]);
                    e = Split(sr.ReadLine());
                }

                bool z = true;

                dfDataH = e;

                // Read the data rows, these look something like
                // mz, rt, x, y, z,    1, 2, 3, 4, ...
                // [ var info block ]  [ data block ]
                while (true)
                {
                    if (sr.EndOfStream)
                    {
                        e = new string[0];
                    }
                    else
                    {
                        e = Split(sr.ReadLine());
                    }

                    if (e.Length <= 1)
                    {
                        break;
                    }

                    if (z)
                    {
                        dfInfo.AddRow(e, f - 1, e.Length - 1);
                        z = false;
                    }

                    dfVarInfo.AddRow(e, 0, f - 1);
                    dfData.AddRow(e, f, e.Length - 1);
                }
            }

            if (dfData.NumRows != dfVarInfo.NumRows)
            {
                throw new Exception("dfData.NumRows != dfVarInfo.NumRows");
            }

            if (dfData.NumColumns != dfInfo.NumColumns)
            {
                throw new Exception("dfData.NumRows != dfVarInfo.NumRows");
            }

            // Set the row headers
            dfVarInfo.SetRowHeaders(Contig(dfVarInfo.NumRows).ToArray(), 0, z => "C" + z);
            dfInfo.SetRowHeaders(dfInfoH.ToArray(), 0, z => z);
            dfData.SetRowHeaders(dfVarInfo.RowHeaders, 0, z => z);

            // Set the col headers
            dfVarInfo.SetColHeaders(dfDataH, 0, z => z);
            dfData.SetColHeaders(dfDataH, f, z => "F" + z);
            dfInfo.SetColHeaders(dfData.ColHeaders, 0, z => z);

            dfData.Save(fnDataOut, true);
            dfVarInfo.Save(fnVarInfoOut, false);
            dfInfo.Save(fnInfoOut, true);

            rp.AddOutput("Value matrix", fnDataOut);
            rp.AddOutput("Variable info", fnVarInfoOut);
            rp.AddOutput("Observation info", fnInfoOut);
        }

        private IEnumerable<string> Contig(int p)
        {
            for (int n = 1; n <= p; n++)
            {
                yield return n.ToString();
            }
        }

        private int Propogate(string[] e)
        {
            string x = null;
            int r = -1;

            for (int n = 0; n < e.Length; n++)
            {
                if (e[n] != "")
                {
                    if (x == null)
                    {
                        r = n;
                    }

                    x = e[n];
                }
                else
                {
                    if (x != null)
                    {
                        e[n] = x;
                    }
                }
            }

            return r;
        }

        private static string[] Split(string l)
        {
            return l.Split(',');
        }
    }
}
