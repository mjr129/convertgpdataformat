﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Rename files")]
    [Description("Renames files.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class RenameFiles : Transformation
    {
        [EditorAttribute(typeof(OpenMultipleFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Filenames")]
        [Category("Inputs")]
        [Description("Files to read")]
        [TypeConverter(typeof(StringListConvertor))]
        [DefaultValue("")]
        public string[] InputFiles { get; set; }

        public override void Run(ResultsPackage rp)
        {
            foreach (string file in InputFiles)
            {
                string dir = Path.GetDirectoryName(file);
                string fn = Path.GetFileNameWithoutExtension(file);
                string ext = Path.GetExtension(file);

                var a = Split(fn, " - ");
                var b = Split(a.Item2, " CR-");

                string nfn = a.Item1 + " - CR-" + b.Item2 + " - " + b.Item1.Replace("for", "").Replace("  ", " ").Trim();

                nfn = Path.Combine(dir, nfn + ext);

                File.Move(file, nfn);
            }

            rp.AddInfo("Renamed", InputFiles.Length);
        }

        private Tuple<string, string> Split(string fn, string p)
        {
            string[] s = fn.Split(new string[] { p }, 2, StringSplitOptions.None);
            return new Tuple<string, string>(s[0], s[1]);
        }
    }
}
