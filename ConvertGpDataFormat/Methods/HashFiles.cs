﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Hash files")]
    [Description("Calculates SHA256 hashes of files")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class HashFiles : Transformation
    {
        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Directory containing the input files")]
        [DefaultValue("")]
        public string InputDirectory { get; set; }

        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Outputs")]
        [Description("Macros are accepted based on InputDirectory.")]
        [DefaultValue("{DIR}Hashes.txt")]
        public string OutputFile { get; set; }

        [Category("Inputs")]
        [Description("File pattern to consider")]
        [DefaultValue("*.*")]
        public string SearchPattern { get; set; }

        [Category("Inputs")]
        [Description("Recurse into subfolders")]
        [DefaultValue(false)]
        public bool Recurse { get; set; }

        [Category("Inputs")]
        [Description("How many bytes of the file to hash, maximum")]
        [DefaultValue(1024 * 1024)]
        public int ConsiderBytes { get; set; }

        public HashFiles()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public override void Run(ResultsPackage rp)
        {
            string outFile = base.Ffn(OutputFile, InputDirectory);

            using (StreamWriter sw = new StreamWriter(outFile))
            {
                foreach (string fileName in Directory.EnumerateFiles(InputDirectory, SearchPattern, Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                {
                    if (fileName.ToUpper() != outFile.ToUpper())
                    {
                        string hash = Hash(fileName);

                        sw.WriteLine(fileName + "\t" + ConsiderBytes + "\t" + hash);
                    }
                }
            }

            rp.AddOutput(outFile);
        }

        private string Hash(string fileName)
        {
            using (FileStream r = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                int size = ConsiderBytes;

                byte[] buf = new byte[size];

                size = r.Read(buf, 0, size);

                SHA256 sha = SHA256.Create();

                byte[] hash = sha.ComputeHash(buf, 0, size);

                return Convert.ToBase64String(hash);
            }
        }
    }
}
