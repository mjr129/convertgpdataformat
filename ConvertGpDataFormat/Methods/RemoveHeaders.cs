﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Remove headers")]
    [Description("Remove the headers from a CSV file.")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class RemoveHeaders : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File to read in")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH}{FILE}-noheaders{EXT}")]
        public string OutputFile { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [Category("Inputs")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InputCsvSeparator { get; set; }

        [Description("If set when removing row headers any additional CSV delimiters encountered after the first one will also be trimmed if they occur in succession.")]
        [Category("Outputs")]
        [DefaultValue(false)]
        public bool TrimHeaders { get; set; }

        [Description("Whether to remove the row headers")]
        [Category("Outputs")]
        [DefaultValue(false)]
        public bool RemoveRowHeaders { get; set; }

        [Description("Whether to remove the column headers")]
        [Category("Outputs")]
        [DefaultValue(false)]
        public bool RemoveColumnHeaders { get; set; }

        public RemoveHeaders()
        {
            InputCsvSeparator = "{TAB}{SPACE},";
            OutputFile = "{PATH}{FILE}-noheaders{EXT}";
        }

        public override void Run(ResultsPackage rp)
        {
            string outfile = base.Ffn(OutputFile, InputFile);
            char[] csvsep = base.Escape(InputCsvSeparator).ToCharArray();

            using (StreamReader sr = new StreamReader(InputFile))
            {
                using (StreamWriter sw = new StreamWriter(outfile))
                {
                    if (RemoveColumnHeaders && !sr.EndOfStream)
                    {
                        sr.ReadLine();
                    }

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (RemoveRowHeaders)
                        {
                            int index = line.IndexOfAny(csvsep);

                            line = line.Substring(index + 1);

                            if (TrimHeaders)
                            {
                                line = line.TrimStart(csvsep);
                            }
                        }

                        sw.WriteLine(line);
                    }
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
