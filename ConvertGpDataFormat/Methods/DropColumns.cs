﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Column = System.Tuple<int, decimal>;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Drop columns")]
    [Description("Reads statistics file (see Column Statistics) and drops columns based on their value")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class DropColumns : CsvInOutTransformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [DisplayName("Filename")]
        [Category("Info file")]
        [Description("File to read")]
        [DefaultValue("")]
        public string InfoFile { get; set; }

        [Description("Any and all of these will be treated as CSV separators.")]
        [DisplayName("CSV separator(s)")]
        [Category("Info file")]
        [DefaultValue("{TAB}{SPACE},")]
        public string InfoCsvSeparator { get; set; }

        [Description("Ignore empty cells (use if data has been justified by adding spaces, etc.).")]
        [DisplayName("Ignore empty columns")]
        [Category("Info file")]
        [DefaultValue(false)]
        public bool InfoDropEmpties { get; set; }

        [Description("Row values to read")]
        [DisplayName("Info row")]
        [Category("Info file")]
        [DefaultValue("mean")]
        public string InfoHeader { get; set; }

        [Description("Sort high to low (i.e. keep highest)")]
        [DisplayName("Reverse sort")]
        [Category("Info file")]
        [DefaultValue(true)]
        public bool ReverseSort { get; set; }

        [Description("Fraction of values to retain")]
        [DisplayName("Retain amount")]
        [Category("Info file")]
        [DefaultValue(0.25)]
        public double RetainAmount { get; set; }

        protected override void ProcessFiles(RunArgs e)
        {
            string infoFile = base.Ffn(InfoFile, e.InputFile);
            char[] infoCsvSep = base.Escape(InfoCsvSeparator).ToCharArray();
            StringSplitOptions infoSso = InfoDropEmpties ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            // Load values
            List<Column> vals = null;

            using (StreamReader sr = new StreamReader(infoFile))
            {
                while (!sr.EndOfStream)
                {
                    string[] fields = base.ReadLine(sr, infoCsvSep, infoSso);

                    if (fields[0] == InfoHeader)
                    {
                        vals = new List<Column>();

                        for (int i = 1; i < fields.Length; i++)
                        {
                            vals.Add(new Column(i - 1, decimal.Parse(fields[i])));
                        }

                        break;
                    }
                }

                if (vals == null)
                {
                    throw new Exception("Cannot find row: " + InfoHeader);
                }
            }

            // Sort
            List<Column> svals = new List<Column>(vals);

            if (ReverseSort)
            {
                svals.Sort((x, y) => y.Item2.CompareTo(x.Item2));
            }
            else
            {
                svals.Sort((x, y) => x.Item2.CompareTo(y.Item2));
            }

            // Find cutoff
            int cutoffIndex = (int)((double)svals.Count * RetainAmount);
            e.Meta.AddInfo("Old columns", svals.Count);
            e.Meta.AddInfo("New columns", cutoffIndex);
            e.Meta.AddInfo("New columns (%)", (cutoffIndex * 100) / svals.Count);
            decimal cutoff = svals[cutoffIndex].Item2;
            int lineCount = 0;

            using (StreamReader sr = new StreamReader(e.InputFile))
            {
                using (StreamWriter sw = new StreamWriter(e.OutputFile))
                {
                    while (!sr.EndOfStream)
                    {
                        e.Meta.ReportProgress(sr);

                        string[] fields = ReadLine(e, sr);

                        if (fields != null)
                        {
                            Debug.Assert(fields.Length == vals.Count, "Column counts in info and data file are different.");

                            bool writeComma = false;

                            for (int i = 0; i < fields.Length; i++)
                            {
                                Debug.Assert(vals[i].Item1 == i);
                                bool write;

                                if (ReverseSort)
                                {
                                    write = (vals[i].Item2 >= cutoff);
                                }
                                else
                                {
                                    write = (vals[i].Item2 <= cutoff);
                                }

                                if (write)
                                {
                                    if (writeComma)
                                    {
                                        sw.Write(e.CsvSeparatorOut);
                                    }

                                    sw.Write(fields[i]);
                                    writeComma = true;
                                }
                            }

                            lineCount++;
                            sw.WriteLine();
                        }
                    }
                }
            }

            e.Meta.AddInfo("Line count", lineCount);
        }
    }
}
