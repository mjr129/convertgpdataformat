﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("NMR Bin Identification")]
    [Description("Identify NMR bins using BinStart and NoisyBins")]
    [Folder(FolderType.Nmr)]
    [Serializable]
    class BinIdentification : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File containing the bin starts for all bins")]
        [DefaultValue("")]
        public string AllBinsFile { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File containing the bin starts for the noisy (excluded) bins")]
        [DefaultValue("")]
        public string NoisyBinsFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file (CSV). Macros are accepted (AllBins = 1, NoiseyBins = 2).")]
        [DefaultValue("{PATH1}NormalBins{EXT1}")]
        public string OutputFile { get; set; }

        [Category("Outputs")]
        [Description("Which bins to output")]
        [DefaultValue(WhichToOutput.Normal)]
        public WhichToOutput OutputMode { get; set; }

        public BinIdentification()
        {
            base.ApplyDefaultsFromAttributes();
        }

        public enum WhichToOutput
        {
            All,
            Normal,
            Noisy
        }

        public override void Run(ResultsPackage rp)
        {
            string outFile = base.Ffn(OutputFile, AllBinsFile, NoisyBinsFile);
            List<double> allBins = ReadAllData(AllBinsFile);
            List<double> noisyBins = ReadAllData(NoisyBinsFile);
            List<Bin> bins = new List<Bin>();

            for (int n = 0; n < allBins.Count - 1; n++)
            {
                Bin b = new Bin(allBins[n], allBins[n + 1]);
                bins.Add(b);
            }

            foreach (double d in noisyBins)
            {
                foreach (Bin b in bins)
                {
                    if (b.start == d)
                    {
                        b.isNoisy = true;
                    }
                }
            }

            int normalCount = 0;
            int outputCount = 0;

            foreach (Bin b in bins)
            {
                if (!b.isNoisy)
                {
                    normalCount++;
                }
            }

            using (StreamWriter sw = new StreamWriter(outFile))
            {
                sw.WriteLine("Index,Start,End,IsNoisy");
                bool match = OutputMode == WhichToOutput.Noisy ? true : false;

                foreach (Bin b in bins)
                {
                    if (OutputMode == WhichToOutput.All || b.isNoisy == match)
                    {
                        outputCount++;
                        sw.WriteLine(outputCount.ToString() + "," + b.start + "," + b.end + "," + (b.isNoisy ? 1 : 0));
                    }
                }
            }

            rp.AddInfo("All Bins", allBins.Count);
            rp.AddInfo("Noisy Bins", noisyBins.Count);
            rp.AddInfo("Normal Bins", normalCount);
            rp.AddInfo("Output Bins", outputCount);
            rp.AddOutput(outFile);
        }

        class Bin
        {
            public double start;
            public double end;
            public bool isNoisy;

            public Bin(double start, double end)
            {
                this.start = start;
                this.end = end;
            }
        }

        private List<double> ReadAllData(string fileName)
        {
            char[] csvSep = " \t,".ToCharArray();

            List<double> result = new List<double>();

            using (StreamReader sr = new StreamReader(fileName))
            {
                while (!sr.EndOfStream)
                {
                    string[] elements = sr.ReadLine().Split(csvSep, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string element in elements)
                    {
                        result.Add(double.Parse(element));
                    }
                }
            }

            return result;
        }
    }
}
