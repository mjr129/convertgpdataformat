﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Find and replace text")]
    [Description("Finds and replaces text, line by line")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class FindAndReplace : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File to read in")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Inputs")]
        [Description("Text to find")]
        [DefaultValue("")]
        public string Find { get; set; }

        [Category("Behaviour")]
        [Description("Whether to use regular expressions. If not only basic macros will be accepted ({SPACE} = space, {TAB} = tab)")]
        [DisplayName("Use regular expressions")]
        [DefaultValue(true)]
        public bool IsRegex { get; set; }

        [Category("Behaviour")]
        [Description("Whether to ignore case (only when using regular expressions)")]
        [DisplayName("Ignore case (regex only)")]
        [DefaultValue(true)]
        public bool RegexIgnoreCase { get; set; }

        [Category("Outputs")]
        [Description("Replacement text")]
        [DefaultValue("")]
        public string Replace { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("File to write out")]
        [DefaultValue("")]
        public string OutputFile { get; set; }

        public FindAndReplace()
        {
            InputFile = "";
            Find = "";
            IsRegex = true;
            RegexIgnoreCase = true;
            Replace = "";
            OutputFile = "";
        }

        public override void Run(ResultsPackage rp)
        {
            string find = IsRegex ? Find : base.Escape(Find);
            string replace = IsRegex ? Replace : base.Escape(Replace);
            string outfile = base.Ffn(OutputFile, InputFile);

            using (StreamReader sr = new StreamReader(InputFile))
            {
                using (StreamWriter sw = new StreamWriter(outfile))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();

                        if (IsRegex)
                        {
                            RegexOptions options = RegexOptions.Singleline
                                                 | (RegexIgnoreCase ? RegexOptions.IgnoreCase : RegexOptions.None);
                            line = Regex.Replace(line, find, replace, options);
                        }
                        else
                        {
                            line = line.Replace(find, replace);
                        }

                        sw.WriteLine(line);
                    }
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
