﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Column statistics")]
    [Description("Gets values for each column: mean, sd, sum, variance, count")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class ColumnStatistics : CsvInOutTransformation
    {
        [DisplayName("Calculate variance and sd")]
        [Category("Processing")]
        [Description("Whether to calculate: variance, sd. This doubles the time taken for the calculation as the file has to be read twice.")]
        [DefaultValue(true)]
        public bool CalculateVariance { get; set; }

        protected override void ProcessFiles(RunArgs e)
        {
            List<decimal> sums = new List<decimal>();
            List<decimal> mins = new List<decimal>();
            List<decimal> maxes = new List<decimal>();
            List<int> nonzero = new List<int>();
            int lineCount = 0;
            int total = 0;

            using (StreamReader sr = new StreamReader(e.InputFile))
            {
                while (!sr.EndOfStream)
                {
                    e.Meta.ReportProgress(sr, 0, 2);

                    var a = ReadLine(e, sr);

                    if (a != null)
                    {
                        lineCount++;

                        for (int i = 0; i < a.Length; i++)
                        {
                            if (a[i].Length == 0 && sums.Count < (i + 1))
                            {
                                // Allow empty fields but only at the end of lines
                                continue;
                            }

                            if (sums.Count < (i + 1))
                            {
                                sums.Add(0);
                                mins.Add(decimal.MaxValue);
                                maxes.Add(decimal.MinValue);
                                nonzero.Add(0);
                            }

                            var v = decimal.Parse(a[i]);
                            sums[i] += v;
                            mins[i] = Math.Min(mins[i], v);
                            maxes[i] = Math.Max(maxes[i], v);

                            if (v != 0)
                            {
                                nonzero[i] += 1;
                            }
                        }
                    }
                }
            }

            List<decimal> mean = new List<decimal>();

            foreach (var v in sums)
            {
                mean.Add(v / lineCount);
            }

            List<decimal> var = null;
            List<decimal> sd = null;
            List<decimal> cv = null;

            if (CalculateVariance)
            {
                var = new List<decimal>();

                using (StreamReader sr = new StreamReader(e.InputFile))
                {
                    while (!sr.EndOfStream)
                    {
                        e.Meta.ReportProgress(sr, 1, 2);

                        var a = ReadLine(e, sr);

                        if (a != null)
                        {
                            for (int i = 0; i < a.Length; i++)
                            {
                                if (a[i].Length == 0 && sums.Count < (i + 1))
                                {
                                    // Allow empty fields but only at the end of lines
                                    continue;
                                }

                                if (var.Count < (i + 1))
                                {
                                    var.Add(0);
                                }

                                var v = decimal.Parse(a[i]);
                                v -= mean[i];
                                var[i] += (v * v);
                            }
                        }
                    }
                }

                sd = new List<decimal>();
                cv = new List<decimal>();

                for (int i = 0; i < var.Count; i++)
                {
                    var[i] /= lineCount;
                    sd.Add((decimal)Math.Sqrt((double)var[i]));
                    cv.Add(sd[i] / mean[i]);
                }
            }

            using (StreamWriter sw = new StreamWriter(e.OutputFile))
            {
                WriteLine(sw, Sequence(1, sums.Count, 1), e.CsvSeparatorOut, "Index");
                WriteLine(sw, Enumerable.Repeat(lineCount, sums.Count), e.CsvSeparatorOut, "Count");
                WriteLine(sw, nonzero, e.CsvSeparatorOut, "NonzeroCount");
                WriteLine(sw, sums, e.CsvSeparatorOut, "Sum");
                WriteLine(sw, mins, e.CsvSeparatorOut, "Min");
                WriteLine(sw, maxes, e.CsvSeparatorOut, "Max");
                WriteLine(sw, mean, e.CsvSeparatorOut, "Mean");

                if (CalculateVariance)
                {
                    WriteLine(sw, var, e.CsvSeparatorOut, "Variance");
                    WriteLine(sw, sd, e.CsvSeparatorOut, "sd");
                    WriteLine(sw, cv, e.CsvSeparatorOut, "cv");
                }
            }

            e.Meta.AddInfo("Total", total);
            e.Meta.AddInfo("Input line count", lineCount);
            e.Meta.AddInfo("Column count", sums.Count);
        }

        private IEnumerable<int> Sequence(int from, int to, int step)
        {
            for (int n = from; n <= to; n += step)
            {
                yield return n;
            }
        }
    }
}
