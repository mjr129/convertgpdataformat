﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ConvertGpDataFormat.Methods
{
    [Serializable]
    abstract class Transformation
    {
        public ResultsPackage Run(Action<int, int> progress)
        {
            ResultsPackage rp = new ResultsPackage(progress);

            Run(rp);

            return rp;
        }

        public abstract void Run(ResultsPackage rp);

        protected string FfnDir(string pattern, string directory)
        {
            return Ffn(pattern, Path.Combine(directory, "dir.dir"));
        }

        protected string Ffn(string pattern, params string[] originals)
        {
            string result = pattern;

            result = ReplaceFile(result, "", originals[0]);

            for (int n = 0; n < originals.Length; n++)
            {
                result = ReplaceFile(result, n.ToString(), originals[n]);
            }

            string commonPrefix = CommonPrefix(originals);
            result = ReplaceFile(result, "*", commonPrefix);

            string guid = Guid.NewGuid().ToString();
            result = result.Replace("{GUID}", guid);

            DateTime now = DateTime.Now;
            result = result.Replace("{DATE}", now.ToString("yyyy-MM-dd"));
            result = result.Replace("{TIME}", now.ToString("HH-mm-ss"));
            result = result.Replace("{DATETIME}", now.ToString("yyyy-MM-dd HH-mm-ss"));

            result = result.Replace("{{}", "{");
            result = result.Replace("{}}", "}");

            return result;
        }

        private static string ReplaceFile(string result, string id, string original)
        {
            string fileDir = Terminate(Path.GetDirectoryName(original));
            string fileName = Path.GetFileNameWithoutExtension(original);
            string fileExt = Path.GetExtension(original);
            string dirName = Terminate(original);

            result = result.Replace("{PATH" + id + "}", fileDir);   // c:\folder\
            result = result.Replace("{FILE" + id + "}", fileName);  // file
            result = result.Replace("{EXT" + id + "}", fileExt);    // .ext
            result = result.Replace("{DIR" + id + "}", dirName);    // c:\folder\file.ext\
            result = result.Replace("{NAME" + id + "}", original);  // c:\folder\file.txt
            return result;
        }

        private static string CommonPrefix(string[] originals)
        {
            for (int n = 0; ; n++)
            {
                for (int o = 0; o < originals.Length; o++)
                {
                    if (n >= originals[o].Length)
                    {
                        return originals[0].Substring(0, n);
                    }

                    if (o != 0)
                    {
                        if (originals[o][n] != originals[o - 1][n])
                        {
                            return originals[0].Substring(0, n);
                        }
                    }
                }
            }
        }

        private static string Terminate(string fileDir)
        {
            if (!fileDir.EndsWith(Path.DirectorySeparatorChar.ToString()) && !fileDir.EndsWith(Path.AltDirectorySeparatorChar.ToString()))
            {
                fileDir += Path.DirectorySeparatorChar;
            }
            return fileDir;
        }

        protected string Escape(string pattern)
        {
            string result = pattern;

            result = result.Replace("{TAB}", "\t");
            result = result.Replace("{SPACE}", " ");
            result = result.Replace("{{}", "{");
            result = result.Replace("{}}", "}");

            return result;
        }

        protected void ApplyDefaultsFromAttributes()
        {
            foreach (PropertyInfo property in this.GetType().GetProperties())
            {
                var attr = property.GetCustomAttribute<DefaultValueAttribute>(true);

                if (attr != null)
                {
                    property.SetValue(this, attr.Value);
                }
            }
        }

        public StreamWriter SaveFile(string fnLeft)
        {
            string dir = Path.GetDirectoryName(fnLeft);

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return new StreamWriter(fnLeft);
        }
    }
}
