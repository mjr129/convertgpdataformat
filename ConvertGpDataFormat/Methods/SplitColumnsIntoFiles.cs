﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Split columns into files")]
    [Description("Splits columns into two files")]
    [Folder(FolderType.Manip)]
    [Serializable]
    class SplitColumnsIntoFiles : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File containing data (any format, one observation per line)")]
        [DefaultValue("")]
        public string InputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file")]
        [DefaultValue("{PATH}{FILE}-Meta{EXT}")]
        public string LeftOutputFile { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file")]
        [DefaultValue("{PATH}{FILE}-Data{EXT}")]
        public string RightOutputFile { get; set; }

        [Category("Behaviour")]
        [Description("Which columns appear in the left file (all other columns will appear in the right file)")]
        [DefaultValue("0, 1, 2, 3")]
        public string LeftColumns
        {
            get
            {
                return String.Join(", ", _leftColumns ?? new int[0]);
            }
            set
            {
                string[] elements = value.Split(", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                List<int> result = new List<int>();
                foreach (string element in elements)
                {
                    int v;
                    if (int.TryParse(element, out v))
                    {
                        result.Add(v);
                    }
                }
                _leftColumns = result.ToArray();
            }
        }
        private int[] _leftColumns = { 0, 1, 2, 3 };

        [Category("Behaviour")]
        [Description("Input file delimiters")]
        [DefaultValue(" \t,")]
        public string InputFileDelimiters { get; set; }

        [Category("Behaviour")]
        [Description("Output file delimiter")]
        [DefaultValue(",")]
        public string OutputFileDelimiter { get; set; }

        public SplitColumnsIntoFiles()
        {
            InputFile = "";
            LeftOutputFile = "{PATH}{FILE}-Meta{EXT}";
            RightOutputFile = "{PATH}{FILE}-Data{EXT}";
            InputFileDelimiters = " \t,";
            OutputFileDelimiter = ",";
        }

        public override void Run(ResultsPackage rp)
        {
            if (_leftColumns == null)
            {
                _leftColumns = new int[0];
            }

            int[] columns = new int[_leftColumns.Length];
            Array.Copy(_leftColumns, columns, _leftColumns.Length);
            Array.Sort(columns);

            string fnLeft = Ffn(LeftOutputFile, InputFile);
            string fnRight = Ffn(RightOutputFile, InputFile);

            using (StreamReader sr = new StreamReader(InputFile))
            {
                using (StreamWriter sw1 = base.SaveFile(fnLeft))
                {
                    using (StreamWriter sw2 = base.SaveFile(fnRight))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] input = line.Split(OutputFileDelimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                            List<string> left = new List<string>();
                            List<string> right = new List<string>(input);

                            for (int offset = 0; offset < columns.Length; offset++)
                            {
                                int index = columns[offset] - offset;

                                left.Add(right[index]);
                                right.RemoveAt(index);
                            }

                            sw1.WriteLine(string.Join(OutputFileDelimiter, left.ToArray()));
                            sw2.WriteLine(string.Join(OutputFileDelimiter, right.ToArray()));
                        }
                    }
                }
            }

            rp.AddOutput(fnLeft);
            rp.AddOutput(fnRight);
        }
    }
}
