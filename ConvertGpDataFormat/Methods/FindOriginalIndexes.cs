﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Find Original Indexes")]
    [Description("Lists the indexes of the lines of one file in another")]
    [Folder(FolderType.Misc)]
    [Serializable]
    class FindOriginalIndexes : Transformation
    {
        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the left side of the lines are taken from")]
        [DefaultValue("")]
        public string Original { get; set; }

        [EditorAttribute(typeof(OpenFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("File the right side of the lines are taken from")]
        [DefaultValue("")]
        public string New { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Output file. Macros are accepted.")]
        [DefaultValue("{PATH1}{FILE1}+{FILE2}{EXT1}")]
        public string Results { get; set; }

        public override void Run(ResultsPackage rp)
        {
            string outfile = base.Ffn(Results, New);
            string[] orig = File.ReadAllLines(Original);

            using (StreamReader sr = new StreamReader(New))
            using (StreamWriter sw = new StreamWriter(outfile))
            {
                while (!sr.EndOfStream)
                {
                    string l = sr.ReadLine();

                    for (int n = 0; n <= orig.Length; n++)
                    {
                        if (n == orig.Length)
                        {
                            sw.WriteLine("?");
                        }

                        if (orig[n] == l)
                        {
                            sw.WriteLine(n);
                            break;
                        }
                    }
                }
            }

            rp.Add("Output", outfile, ResultsType.FileName);
        }
    }
}
