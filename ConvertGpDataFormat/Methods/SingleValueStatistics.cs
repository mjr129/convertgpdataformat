﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertGpDataFormat.Methods
{
    [DisplayName("Single value statistics")]
    [Description("Calculates: min, max, average, total, count. Reads all comma separated data in dataset as one vector.")]
    [Folder(FolderType.Gp)]
    [Serializable]
    class SingleValueStatistics : Transformation
    {
        [EditorAttribute(typeof(OpenFolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Category("Inputs")]
        [Description("Directory will be recursivley searched for best_program.csv files.")]
        [DefaultValue("")]
        public string InputDirectory { get; set; }

        [Category("Outputs")]
        [EditorAttribute(typeof(SaveFileNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [Description("Created file. Macros are accepted.")]
        [DefaultValue("{PATH}STATS.TXT")]
        public string OutputFile { get; set; }

        [Category("Inputs")]
        [Description("Name of input files")]
        [DefaultValue("_summary.csv")]
        public string InputFile { get; set; }

        [Category("Inputs")]
        [Description("Name of input CSV key")]
        [DefaultValue("Fitness seen")]
        public string InputKey { get; set; }

        public SingleValueStatistics()
        {
            InputFile = "_summary.csv";
            InputKey = "Fitness seen";
        }

        public override void Run(ResultsPackage rp)
        {
            string[] files = Directory.GetFiles(InputDirectory, InputFile, SearchOption.AllDirectories);
            string outfile = Ffn(OutputFile, Path.Combine(InputDirectory, "dir.txt"));
            double min = double.MaxValue;
            double max = double.MinValue;
            double total = 0;
            int count = 0;
            bool valid = true;

            using (StreamWriter sw = new StreamWriter(outfile))
            {
                foreach (string file in files)
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] elements = line.Split(",".ToCharArray(), 2);

                            if (elements.Length == 2)
                            {
                                if (elements[0].Trim() == InputKey)
                                {
                                    string value = elements[1].Trim();
                                    sw.WriteLine(value);

                                    double v;
                                    if (double.TryParse(value, out v))
                                    {
                                        min = Math.Min(min, v);
                                        max = Math.Max(max, v);
                                        total += v;
                                        count++;
                                    }
                                    else
                                    {
                                        valid = false;
                                    }
                                }
                            }
                        }
                    }
                }

                if (valid)
                {
                    rp.AddInfo("Min", min);
                    rp.AddInfo("Max", max);
                    rp.AddInfo("Total", total);
                    rp.AddInfo("Count", count);
                    rp.AddInfo("Mean", (total / count));
                }
            }

            rp.AddOutput(outfile);
        }
    }
}
