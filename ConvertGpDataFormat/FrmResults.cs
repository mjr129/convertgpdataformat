﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertGpDataFormat
{
    public partial class FrmResults : Form
    {
        ResultsPackage _rp;

        public static void Show(IWin32Window owner, ResultsPackage rp, string title)
        {
            using (var frm = new FrmResults(rp, title))
            {
                frm.ShowDialog(owner);
            }
        }

        public FrmResults(ResultsPackage rp, string title)
        {
            InitializeComponent();

            Text = title;

            Font boldFont = new Font(Font, FontStyle.Bold);

            _rp = rp;

            foreach (ResultsPackage.Result r in rp.Get())
            {
                Label l1 = new Label();
                l1.Text = r.name;
                l1.Font = boldFont;
                l1.AutoSize = true;

                tableLayoutPanel1.Controls.Add(l1);

                if (r.type == ResultsType.FileName)
                {
                    LinkLabel l2 = new LinkLabel();
                    l2.Text = r.data;
                    l2.AutoSize = true;
                    l2.Click += fileName_Click;
                    // l2.LinkColor = l2.ForeColor;
                    l2.LinkBehavior = LinkBehavior.HoverUnderline;
                    tableLayoutPanel1.Controls.Add(l2);
                }
                else if (r.type == ResultsType.Report)
                {
                    LinkLabel l2 = new LinkLabel();
                    l2.Text = "View...";
                    l2.AutoSize = true;
                    l2.LinkBehavior = LinkBehavior.HoverUnderline;
                    l2.Tag = r.data;
                    l2.Click += report_Click;
                    tableLayoutPanel1.Controls.Add(l2);
                }
                else
                {
                    Label l2 = new Label();
                    l2.Text = r.data;
                    l2.AutoSize = true;
                    tableLayoutPanel1.Controls.Add(l2);
                }
            }
        }

        void report_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, ((string)((LinkLabel)sender).Tag), "View report", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        string fileNameClicked;

        void fileName_Click(object sender, EventArgs e)
        {
            LinkLabel ll = (LinkLabel)sender;
            fileNameClicked = ll.Text;

            contextMenuStrip1.Show(ll, 0, ll.Height);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(_rp.ToString());
        }

        private void viewInExplorerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", "/select,\"" + fileNameClicked + "\"");
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(fileNameClicked);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(fileNameClicked);
        }
    }
}
