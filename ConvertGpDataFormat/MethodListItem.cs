﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ConvertGpDataFormat
{
    class MethodListItem
    {
        public Type Class;

        public MethodListItem(Type type)
        {
            Class = type;
        }

        public override string ToString()
        {
            // return GetFolderDisplayName() + " \\ " + GetDisplayName();
            return GetDisplayName();
        }

        public string GetFolderDisplayName()
        {
            string fname;
            FolderAttribute fattr = Class.GetCustomAttribute<FolderAttribute>();

            if (fattr != null)
            {
                var folder = fattr.Folder;

                FolderNameAttribute dattr = typeof(FolderType).GetMember(folder.ToString())[0].GetCustomAttribute<FolderNameAttribute>();

                if (dattr != null)
                {
                    fname = dattr.Name;
                }
                else
                {
                    fname = "Untitled folder (" + folder.ToString() + ")";
                }
            }
            else
            {
                fname = "Unspecified folder";
            }
            return fname;
        }

        public string GetDisplayName()
        {
            string name;
            DisplayNameAttribute attr = Class.GetCustomAttribute<DisplayNameAttribute>();

            if (attr != null)
            {
                name = attr.DisplayName;
            }
            else
            {
                name = "Unnamed class (" + Class.Name + ")";
            }
            return name;
        }
    }
}
