﻿using ConvertGpDataFormat.Methods;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace ConvertGpDataFormat
{
    public partial class Form1 : Form
    {
        Transformation _selectedTransformation;
        bool _ignoreListChanges;
        string normalsettings;
        string customsettings;
        string batchSettings;

        public Form1()
        {
            InitializeComponent();

            List<MethodListItem> items = new List<MethodListItem>();

            foreach (Module module in Assembly.GetExecutingAssembly().GetModules())
            {
                foreach (Type type in module.GetTypes())
                {
                    if (typeof(Transformation).IsAssignableFrom(type) && !type.IsAbstract)
                    {
                        items.Add(new MethodListItem(type));
                    }
                }
            }

            items.Sort((x, y) => (string.Compare(x.ToString(), y.ToString())));

            this._lstTransformations.Items.AddRange(items.ToArray<object>());

            normalsettings = Path.Combine(Application.StartupPath, "Settings\\LastUsed");
            customsettings = Path.Combine(Application.StartupPath, "Settings\\Customised");
            batchSettings = Path.Combine(Application.StartupPath, "Settings\\Batch");

            if (!Directory.Exists(normalsettings)) Directory.CreateDirectory(normalsettings);
            if (!Directory.Exists(customsettings)) Directory.CreateDirectory(customsettings);
            if (!Directory.Exists(batchSettings)) Directory.CreateDirectory(batchSettings);

            string mainConfigFileName = Path.Combine(normalsettings, "config.last.dat");

            try
            {
                if (File.Exists(mainConfigFileName))
                {
                    string typeName = File.ReadAllText(mainConfigFileName);
                    LoadFromFile(typeName, true, null, "", normalsettings);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
                File.Delete(mainConfigFileName);
            }

            // Command line
            string[] args = Environment.GetCommandLineArgs();

            for (int n = 0; n < args.Length; n++)
            {
                string a = args[n];

                if (a == "/t" && n != args.Length - 1)
                {
                    SelectTransformation(args[n + 1]);
                    n++;
                }
                else if (a == "/l")
                {
                    _lstTransformations.Visible = false;
                    label1.Visible = false;
                    _btnMenu.Visible = false;
                    Text = ((MethodListItem)_lstTransformations.SelectedItem).GetDisplayName();
                }
                else if (n != 0)
                {
                    MessageBox.Show("Unknown command line argument: " + a);
                }
            }
        }

        private void ShowTransformationForm()
        {
            var ni = FrmSelectMethod.Show(this, this._lstTransformations.Items.Cast<MethodListItem>());

            if (ni != null)
            {
                SelectTransformation(ni.Class.Name);
            }
        }

        private void SelectTransformation(string name)
        {
            foreach (MethodListItem li in _lstTransformations.Items)
            {
                if (li.Class.Name.ToUpper() == name.ToUpper())
                {
                    _lstTransformations.SelectedItem = li;
                    return;
                }
            }

            MessageBox.Show("Unknown transformation: " + name);
        }

        protected override void OnClosed(EventArgs e)
        {
            SaveToFile("", normalsettings);

            base.OnClosed(e);
        }

        private void SaveToFile(string exName, string dir)
        {
            if (_selectedTransformation == null)
            {
                return;
            }

            try
            {
                if (string.IsNullOrEmpty(exName))
                {
                    exName = "";
                }
                else
                {
                    exName = "." + exName;
                }

                string fileName = Path.Combine(dir, "config." + _selectedTransformation.GetType().Name + exName + ".dat");
                string mainConfigFileName = Path.Combine(dir, "config.last.dat");

                using (FileStream fs = new FileStream(fileName, FileMode.Create))
                {
                    //XmlSerializer xs = new XmlSerializer(_selectedTransformation.GetType());
                    //xs.Serialize(fs, _selectedTransformation);
                    BinaryFormatter bf = new BinaryFormatter();
                    bf.Serialize(fs, _selectedTransformation);
                }

                File.WriteAllText(mainConfigFileName, _selectedTransformation.GetType().Name);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message);
            }
        }

        private void DeleteFile(string exName)
        {
            if (string.IsNullOrEmpty(exName))
            {
                exName = "";
            }
            else
            {
                exName = "." + exName;
            }

            string fileName = Path.Combine(customsettings, "config." + _selectedTransformation.GetType().Name + exName + ".dat");

            File.Delete(fileName);
        }

        private void LoadFromFile(string exName, string dir)
        {
            LoadFromFile(_selectedTransformation.GetType(), false, exName, dir);
        }

        private void LoadFromFile(Type type, bool changeList, string exName, string dir)
        {
            LoadFromFile(type.Name, changeList, type, exName, dir);
        }

        private void LoadFromFile(string typeName, bool changeList, Type fallbackType, string exName, string dir)
        {
            if (string.IsNullOrEmpty(exName))
            {
                exName = "";
            }
            else
            {
                exName = "." + exName;
            }

            string fileName = Path.Combine(dir, "config." + typeName + exName + ".dat");
            _selectedTransformation = null;

            if (File.Exists(fileName))
            {
                try
                {
                    using (FileStream fs = new FileStream(fileName, FileMode.Open))
                    {
                        //XmlSerializer xs = new XmlSerializer(_selectedTransformation.GetType());
                        //_selectedTransformation = (Transformation)xs.Deserialize(fs);
                        BinaryFormatter bf = new BinaryFormatter();
                        _selectedTransformation = (Transformation)bf.Deserialize(fs);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, "Couldn't load the previously saved settings. The transformation method for \"" + Path.GetFileNameWithoutExtension(fileName) + "\" may have been deleted or renamed.\r\n\r\n" + ex.Message);
                    File.Delete(fileName);
                }
            }

            if (_selectedTransformation == null)
            {
                if (fallbackType == null)
                {
                    if (changeList)
                    {
                        _ignoreListChanges = true;
                        _lstTransformations.SelectedIndex = -1;
                        _ignoreListChanges = false;
                    }

                    _propertyGrid.SelectedObject = null;
                    return;
                }

                _selectedTransformation = (Transformation)Activator.CreateInstance(fallbackType);
            }

            _propertyGrid.SelectedObject = _selectedTransformation;

            DescriptionAttribute attr = _selectedTransformation.GetType().GetCustomAttribute<DescriptionAttribute>();
            label1.Text = ((attr != null) ? attr.Description : _selectedTransformation.GetType().Name) + "\r\n ";

            if (changeList)
            {
                foreach (MethodListItem li in _lstTransformations.Items)
                {
                    if (li.Class == _selectedTransformation.GetType())
                    {
                        _ignoreListChanges = true;
                        _lstTransformations.SelectedItem = li;
                        _ignoreListChanges = false;
                        break;
                    }
                }
            }
        }

        private void _lstTransformations_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_ignoreListChanges)
            {
                return;
            }

            SaveToFile("", normalsettings);

            MethodListItem selectedItem = (MethodListItem)_lstTransformations.SelectedItem;

            if (selectedItem != null)
            {
                LoadFromFile(selectedItem.Class, false, "", normalsettings);
            }
            else
            {
                label1.Text = "\r\n ";
            }
        }

        private void ReportProgress(int a, int outOf)
        {
            progressBar1.Maximum = outOf;
            progressBar1.Value = a;
            progressBar1.Refresh();
        }

        private void _btnProcess_Click(object sender, EventArgs e)
        {
            _btnProcess.Text = "PLEASE WAIT";
            _btnProcess.Enabled = false;
            _btnProcess.Refresh();

            SaveToFile("", normalsettings);

            ResultsPackage result = null;

            if (_selectedTransformation != null)
            {
                try
                {
                    progressBar1.Visible = true;
                    result = _selectedTransformation.Run(ReportProgress);
                    progressBar1.Visible = false;
                }
                catch (Exception ex)
                {
                    progressBar1.Visible = false;

                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        if (MessageBox.Show(this, ex.Message + "\r\n\r\nRetry and debug exception?", ex.GetType().Name, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == System.Windows.Forms.DialogResult.Retry)
                        {
                            result = null;
                            progressBar1.Visible = true;
                            result = _selectedTransformation.Run(ReportProgress);
                            progressBar1.Visible = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, ex.Message + "\r\n\r\nAttach a debugger and retry to debug this exception.", ex.GetType().Name, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                if (result != null)
                {
                    FrmResults.Show(this, result, _selectedTransformation.GetType().Name);
                }
            }

            _btnProcess.Text = "Process";
            _btnProcess.Enabled = true;
        }

        private void _btnMenu_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(_btnMenu, new Point(0, _btnMenu.Height));
        }

        private void saveSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void loadSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void defaultsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (_selectedTransformation != null)
            {
                _selectedTransformation = (Transformation)Activator.CreateInstance(_selectedTransformation.GetType());
                _propertyGrid.SelectedObject = _selectedTransformation;
            }
        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            toolStripTextBox1.Text = "";
        }

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SaveToFile(toolStripTextBox1.Text, customsettings);
                contextMenuStrip1.Close(ToolStripDropDownCloseReason.ItemClicked);
                e.Handled = true;
            }
        }

        enum SaveMode
        {
            Save,
            Load,
            Delete,
        }

        private void saveSettingsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            toolStripTextBox1.Text = "(new)";
            PopulateSaves(saveSettingsToolStripMenuItem, SaveMode.Save);
        }

        private void loadSettingsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            PopulateSaves(loadSettingsToolStripMenuItem, SaveMode.Load);
        }

        private void deleteSettingsToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            PopulateSaves(deleteSettingsToolStripMenuItem, SaveMode.Delete);
        }

        private void PopulateSaves(ToolStripMenuItem dropDown, SaveMode saveMode)
        {
            for (int n = 0; n < dropDown.DropDownItems.Count; n++)
            {
                if (dropDown.DropDownItems[n].Tag != null || dropDown.DropDownItems[n].Text == "placeholder")
                {
                    dropDown.Click -= tsmi_Click;
                    dropDown.DropDownItems.RemoveAt(n);
                    n--;
                }
            }

            string prefix = "config." + _selectedTransformation.GetType().Name + ".";
            string suffix = ".dat";
            string filter = prefix + "*" + suffix;

            string[] files = Directory.GetFiles(customsettings, filter, SearchOption.TopDirectoryOnly);

            foreach (string file in files)
            {
                ToolStripMenuItem tsmi = new ToolStripMenuItem(Path.GetFileNameWithoutExtension(file).Substring(prefix.Length));
                tsmi.Tag = saveMode;
                tsmi.Click += tsmi_Click;

                dropDown.DropDownItems.Add(tsmi);
            }

            if (files.Length == 0)
            {
                ToolStripMenuItem tsmi = new ToolStripMenuItem("(none)");
                tsmi.Tag = SaveMode.Save;
                tsmi.Enabled = false;
                tsmi.Click += tsmi_Click;
                dropDown.DropDownItems.Add(tsmi);
            }
        }

        void tsmi_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            SaveMode saveMode = (SaveMode)tsmi.Tag;

            switch (saveMode)
            {
                case SaveMode.Save:
                    SaveToFile(tsmi.Text, customsettings);
                    break;

                case SaveMode.Load:
                    LoadFromFile(tsmi.Text, customsettings);
                    break;

                case SaveMode.Delete:
                    DeleteFile(tsmi.Text);
                    break;
            }
        }

        private void lastSavedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFromFile("", normalsettings);
        }

        private void toolStripTextBox1_Enter(object sender, EventArgs e)
        {

        }

        private void usingFileNameMacrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string text =
@"You can use macros to specify some output file names based on the corresponding input file name, entering the following text to be replaced:
{FILE} = Name of original file (no extension)
{EXT} = Extension of original file (including ""."")
{PATH} = Path to original file (including terminating ""\"")
{NAME} = Unmodified name of original file, the same as {FILE}{PATH}{EXT}.

If the input file is a directory the following may be used instead:
{DIR} = The path of that directory, termianted with ""\"".
{NAME} = Unmodified name of original directory.

Sometimes, if there is more than one input file these can be distinguished using {FILE1}, {FILE2}, etc. Which number is which is given in the description of the field.
Other macros may also be avaialable, again, see the description of the field.";

            MessageBox.Show(this, text, "Using Filename Macros", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
