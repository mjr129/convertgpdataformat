﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertGpDataFormat
{
    class FileNameFilterAttribute : Attribute
    {
        public const string DefaultFilter = "All data files (*.csv, *.txt, *.tsv, *.jgf, *.ssv)|*.csv;*.txt;*.tsv;*.jgf;*.ssv|Comma separated value files (*.csv)|*.csv|Text files (*.txt)|*.txt|Tab separated value files (*.tsv)|*.tsv|JGF files (*.jgf)|*.jgf|Space separated value files (*.ssv)|*.ssv|All files (*.*)|*.*";

        public FileNameFilterAttribute(FileNameFilter filterType)
        {
            FilterType = filterType;
        }

        public FileNameFilter FilterType { get; set; }

        public string GetFilter()
        {
            switch (FilterType)
            {
                case FileNameFilter.All:
                    return "All files (*.*)|*.*";
                
                case FileNameFilter.Csv:
                default:
                    return DefaultFilter;
            }
        }
    }

    enum FileNameFilter
    {
        Csv,
        All,
    }

    abstract class FileNameEditor<T> : UITypeEditor
    {
        public FileNameEditor()
        {
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public static U GetAttribute<U>(ITypeDescriptorContext context)
            where U : Attribute
        {
            foreach (var attr in context.PropertyDescriptor.Attributes)
            {
                if (attr.GetType() == typeof(U))
                {
                    return (U)attr;
                }
            }

            return null;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            using (FileDialog dialog = CreateDialogue())
            {
                dialog.Filter = FileNameFilterAttribute.DefaultFilter;

                foreach (FileNameFilterAttribute f in context.PropertyDescriptor.Attributes.OfType<FileNameFilterAttribute>())
                {
                    dialog.Filter = f.GetFilter();
                }

                InitialiseDialogue((T)value, dialog);

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    value = RetrieveValue(dialog);
                }
            }
            return value;
        }

        protected abstract T RetrieveValue(FileDialog dialog);

        protected abstract void InitialiseDialogue(T value, FileDialog dialog);

        protected abstract FileDialog CreateDialogue();
    }

    abstract class FileNameEditor : FileNameEditor<string>
    {
        protected override void InitialiseDialogue(string fileName, FileDialog dialog)
        {
            dialog.FileName = fileName;
        }

        protected override string RetrieveValue(FileDialog dialog)
        {
            return dialog.FileName;
        }
    }

    class SaveFileNameEditor : FileNameEditor
    {
        protected override FileDialog CreateDialogue()
        {
            return new SaveFileDialog();
        }
    }

    class OpenFileNameEditor : FileNameEditor
    {
        protected override FileDialog CreateDialogue()
        {
            return new OpenFileDialog();
        }
    }

    class OpenMultipleFileNameEditor : FileNameEditor<string[]>
    {
        protected override FileDialog CreateDialogue()
        {
            var result = new OpenFileDialog();
            result.Multiselect = true;
            return result;
        }

        protected override void InitialiseDialogue(string[] fileName, FileDialog dialog)
        {
            dialog.Title = "Select one or more files";
            dialog.FileName = (fileName != null && fileName.Length >= 1) ? fileName[0] : "";
        }

        protected override string[] RetrieveValue(FileDialog dialog)
        {
            return dialog.FileNames;
        }
    }

    class OpenFolderNameEditor : FileNameEditor
    {
        protected override FileDialog CreateDialogue()
        {
            SaveFileDialog result = new SaveFileDialog();
            result.OverwritePrompt = false;
            return result;
        }

        protected override void InitialiseDialogue(string fileName, FileDialog dialog)
        {
            dialog.Title = "Select Folder";
            dialog.FileName = Path.Combine(fileName, "Select Folder");
            dialog.Filter = "Select Folder|Select Folder";
        }

        protected override string RetrieveValue(FileDialog dialog)
        {
            return Path.GetDirectoryName(dialog.FileName);
        }
    }

    class StringListConvertor : ExpandableObjectConverter
    {
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string[]))
            {
                return true;
            }

            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is string[])
            {
                string[] v = (string[])value;
                if (v.Length == 0)
                {
                    return "(None)";
                }
                else if (v.Length == 1)
                {
                    return "(" + v.Length + " file)";
                }
                else
                {
                    return "(" + v.Length + " files)";
                }
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
